package com.quicksilver.utils;

import com.quicksilver.Kit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by Derek on 12/2/2014.
 * Time: 5:32 PM
 */
public class TierUtil {

    private final static ArrayList<ArrayList<ItemStack>> tiers = new ArrayList<>(Arrays.asList(
            registerSwords(),
            registerHelmets(),
            registerChestplates(),
            registerLeggings(),
            registerBoots()
    ));

    public static Kit getDefaultKit() {
        ArrayList<ItemStack> kitItems = new ArrayList<>();
        for (ArrayList<ItemStack> tierList : tiers) {
            kitItems.add(tierList.get(0));
        }
        return new Kit(
                UUID.randomUUID(),
                kitItems.get(0),
                kitItems.get(1),
                kitItems.get(2),
                kitItems.get(3),
                kitItems.get(4)

        );
    }

    public static ItemStack getNextTier(ItemStack itemStack) {
        for (ArrayList<ItemStack> tierList : tiers) {
            if (tierList.contains(itemStack)) {
                int index = tierList.indexOf(itemStack);
                index++;
                if (tierList.size() > index + 1) {
                    return tierList.get(tierList.indexOf(itemStack) + 1);
                }
            }
        }
        return null;
    }

    private static ArrayList<ItemStack> registerBoots() {
        ArrayList<ItemStack> arrayList = new ArrayList<>();

        ItemStack tierOne = new ItemStack(Material.IRON_BOOTS);
        ItemStack tierTwo = new ItemStack(Material.DIAMOND_BOOTS);
        ItemStack tierThree = new ItemStack(Material.DIAMOND_BOOTS);
        ItemStack tierFour = new ItemStack(Material.DIAMOND_BOOTS);
        ItemStack tierFive = new ItemStack(Material.DIAMOND_BOOTS);
        ItemStack tierSix = new ItemStack(Material.DIAMOND_BOOTS);
        ItemStack tierSeven = new ItemStack(Material.DIAMOND_BOOTS);

        tierThree.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        tierFour.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        tierFive.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        tierSix.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        tierSeven.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);

        arrayList.add(tierOne);
        arrayList.add(tierTwo);
        arrayList.add(tierThree);
        arrayList.add(tierFour);
        arrayList.add(tierFive);
        arrayList.add(tierSix);
        arrayList.add(tierSeven);

        return arrayList;
    }

    private static ArrayList<ItemStack> registerChestplates() {
        ArrayList<ItemStack> arrayList = new ArrayList<>();

        ItemStack tierOne = new ItemStack(Material.IRON_CHESTPLATE);
        ItemStack tierTwo = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemStack tierThree = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemStack tierFour = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemStack tierFive = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemStack tierSix = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemStack tierSeven = new ItemStack(Material.DIAMOND_CHESTPLATE);

        tierThree.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        tierFour.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        tierFive.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        tierSix.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        tierSeven.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);

        arrayList.add(tierOne);
        arrayList.add(tierTwo);
        arrayList.add(tierThree);
        arrayList.add(tierFour);
        arrayList.add(tierFive);
        arrayList.add(tierSix);
        arrayList.add(tierSeven);

        return arrayList;
    }

    private static ArrayList<ItemStack> registerHelmets() {
        ArrayList<ItemStack> arrayList = new ArrayList<>();

        ItemStack tierOne = new ItemStack(Material.IRON_HELMET);
        ItemStack tierTwo = new ItemStack(Material.DIAMOND_HELMET);
        ItemStack tierThree = new ItemStack(Material.DIAMOND_HELMET);
        ItemStack tierFour = new ItemStack(Material.DIAMOND_HELMET);
        ItemStack tierFive = new ItemStack(Material.DIAMOND_HELMET);
        ItemStack tierSix = new ItemStack(Material.DIAMOND_HELMET);
        ItemStack tierSeven = new ItemStack(Material.DIAMOND_HELMET);

        tierThree.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        tierFour.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        tierFive.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        tierSix.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        tierSeven.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);

        arrayList.add(tierOne);
        arrayList.add(tierTwo);
        arrayList.add(tierThree);
        arrayList.add(tierFour);
        arrayList.add(tierFive);
        arrayList.add(tierSix);
        arrayList.add(tierSeven);

        return arrayList;
    }

    private static ArrayList<ItemStack> registerLeggings() {
        ArrayList<ItemStack> arrayList = new ArrayList<>();

        ItemStack tierOne = new ItemStack(Material.IRON_LEGGINGS);
        ItemStack tierTwo = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemStack tierThree = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemStack tierFour = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemStack tierFive = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemStack tierSix = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemStack tierSeven = new ItemStack(Material.DIAMOND_LEGGINGS);

        tierThree.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        tierFour.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        tierFive.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        tierSix.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
        tierSeven.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);

        arrayList.add(tierOne);
        arrayList.add(tierTwo);
        arrayList.add(tierThree);
        arrayList.add(tierFour);
        arrayList.add(tierFive);
        arrayList.add(tierSix);
        arrayList.add(tierSeven);

        return arrayList;
    }

    private static ArrayList<ItemStack> registerSwords() {
        ArrayList<ItemStack> arrayList = new ArrayList<>();

        ItemStack tierOne = new ItemStack(Material.IRON_SWORD);
        ItemStack tierTwo = new ItemStack(Material.DIAMOND_SWORD);
        ItemStack tierThree = new ItemStack(Material.DIAMOND_SWORD);
        ItemStack tierFour = new ItemStack(Material.DIAMOND_SWORD);
        ItemStack tierFive = new ItemStack(Material.DIAMOND_SWORD);
        ItemStack tierSix = new ItemStack(Material.DIAMOND_SWORD);
        ItemStack tierSeven = new ItemStack(Material.DIAMOND_SWORD);

        tierThree.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
        tierFour.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
        tierFive.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
        tierSix.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 4);
        tierSeven.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 5);

        arrayList.add(tierOne);
        arrayList.add(tierTwo);
        arrayList.add(tierThree);
        arrayList.add(tierFour);
        arrayList.add(tierFive);
        arrayList.add(tierSix);
        arrayList.add(tierSeven);

        return arrayList;
    }
}
