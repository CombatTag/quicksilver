package com.quicksilver.utils;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Derek on 12/25/2014.
 * Time: 10:22 AM
 */
public class ItemUtil {

    public static int getAvailableAmount(Inventory inventory, ItemStack itemStack) {
        int amount = 0;
        if (itemStack != null) {
            for (ItemStack currentItem : inventory.getContents()) {
                if (currentItem != null) {
                    ItemStack itemStack1 = itemStack.clone();
                    ItemStack currentItem1 = currentItem.clone();
                    itemStack1.setAmount(1);
                    currentItem1.setAmount(1);
                    if (itemStack1.equals(currentItem1)) {
                        amount += itemStack.getMaxStackSize() - currentItem.getAmount();
                    }
                } else {
                    amount += itemStack.getMaxStackSize();
                }
            }
        }
        return amount;
    }

    public static ItemStack getItemStack(ConfigurationSection configurationSection) {
        ItemStack itemStack = new ItemStack(Material.valueOf(configurationSection.getString(ItemPaths.MATERIAL)), configurationSection.getInt(ItemPaths.AMOUNT));
        ItemMeta itemMeta = itemStack.getItemMeta();

        ConfigurationSection enchantSection = configurationSection.getConfigurationSection(ItemPaths.ENCHANTMENTS);
        if (enchantSection != null) {
            for (String string : enchantSection.getKeys(false)) {
                itemStack.addUnsafeEnchantment(Enchantment.getByName(string), enchantSection.getConfigurationSection(string).getInt(ItemPaths.LEVEL));
            }
        }

        if (configurationSection.getString(ItemPaths.DISPLAY_NAME) != null) {
            itemMeta.setDisplayName(configurationSection.getString(ItemPaths.DISPLAY_NAME).replace("&", "\247"));
        }
        if (configurationSection.getStringList(ItemPaths.LORE) != null) {
            List<String> lore = new ArrayList<>();
            for (String string : configurationSection.getStringList(ItemPaths.LORE)) {
                lore.add(string.replace("&", "\247"));
            }
            itemMeta.setLore(lore);
        }

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    public static void setItemStack(ItemStack itemStack, ConfigurationSection configurationSection) {
        configurationSection.set(ItemPaths.AMOUNT, itemStack.getAmount());

        ConfigurationSection enchantSection = configurationSection.getConfigurationSection(ItemPaths.ENCHANTMENTS);
        for (Map.Entry<Enchantment, Integer> entry : itemStack.getEnchantments().entrySet()) {
            enchantSection.set(entry.getKey().getName(), entry.getValue());
        }

        configurationSection.set(ItemPaths.MATERIAL, itemStack.getType().name());

        if (itemStack.hasItemMeta()) {
            if (itemStack.getItemMeta().hasDisplayName()) {
                configurationSection.set(ItemPaths.DISPLAY_NAME, itemStack.getItemMeta().getDisplayName().replace("\247", "&"));
            }

            if (itemStack.getItemMeta().hasLore()) {
                List<String> lore = new ArrayList<>();
                for (String string : itemStack.getItemMeta().getLore()) {
                    lore.add(string.replace("\247", "&"));
                }

                configurationSection.set(ItemPaths.LORE, lore);
            }
        }
    }
}
