package com.quicksilver.utils;

import com.quicksilver.QuickSilver;

/**
 * Created by Derek on 12/13/2014.
 * Time: 8:02 PM
 */
public class ChatUtil {

    public static String t(final String string, final String... values) {
        String message = QuickSilver.p.getMessages().getString(string);
        int index = 0;
        while (message.contains("{" + index + "}")) {
            message = message.replace("{" + index + "}", values[index]);
            index++;
        }
        return message;
    }
}