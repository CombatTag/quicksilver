package com.quicksilver.utils;

/**
 * Created by Derek on 12/25/2014.
 * Time: 10:23 AM
 */
public abstract class ItemPaths {

    public static final String AMOUNT = "amount";
    public static final String DISPLAY_NAME = "displayName";
    public static final String ENCHANTMENTS = "enchantments";
    public static final String LEVEL = "level";
    public static final String LORE = "lore";
    public static final String MATERIAL = "material";
}
