package com.quicksilver.utils;

import com.quicksilver.QuickSilver;
import com.quicksilver.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

import static com.quicksilver.utils.ChatUtil.t;

/**
 * Created by Derek on 12/14/2014.
 * Time: 11:56 AM
 */
public class BankUtil {

    public static void depositQuickSilver(final Inventory inventory, final HumanEntity humanEntity, final Integer... slots) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(QuickSilver.p, new Runnable() {
            @Override
            public void run() {
                User user = QuickSilver.p.getUserManager().getUser(humanEntity);
                for (int slot : slots) {
                    if (!(inventory.getSize() < slot)) {
                        ItemStack bankItem = inventory.getItem(slot);
                        if (bankItem != null) {
                            if (bankItem.getType() == Material.GHAST_TEAR) {
                                if (bankItem.getItemMeta().getDisplayName().equals(ChatColor.BLUE + "QuickSilver")) {
                                    inventory.setItem(slot, null);

                                    user.setBalance(user.getBalance() + bankItem.getAmount());

                                    inventory.setItem(0, getBankItem(user.getBalance()));
                                    user.sendMessage(t("deposit", String.valueOf(bankItem.getAmount()), String.valueOf((int) Math.floor(user.getBalance()))));
                                }
                            }
                        }
                    }
                }
            }
        }, 5);
    }

    public static ItemStack getBankItem(HumanEntity humanEntity) {
        return getBankItem(QuickSilver.p.getUserManager().getUser(humanEntity.getUniqueId()).getBalance());
    }

    public static ItemStack getBankItem(double balance) {
        ItemStack itemStack = new ItemStack(Material.GHAST_TEAR);
        ItemMeta itemMeta = itemStack.getItemMeta();

        itemMeta.setDisplayName(ChatColor.GRAY + "" + ChatColor.BOLD + "" + (int) Math.floor(balance) + "g" + ChatColor.GREEN + ChatColor.BOLD + " QuickSilver");
        itemMeta.setLore(Arrays.asList(ChatColor.GRAY + "Left" + ChatColor.GREEN + " click for raw quicksilver", ChatColor.GRAY + "Right" + ChatColor.GREEN + " click for banknote"));

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    public static ItemStack getBankNote(int worth, int amount) {
        ItemStack itemStack = new ItemStack(Material.PAPER, amount);
        ItemMeta itemMeta = itemStack.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Banknote");
        itemMeta.setLore(Arrays.asList(ChatColor.GRAY + "" + worth + "g " + ChatColor.WHITE + "of QuickSilver"));

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    public static ItemStack getQuickSilver(int amount) {
        ItemStack itemStack = new ItemStack(Material.GHAST_TEAR, amount);
        ItemMeta itemMeta = itemStack.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "QuickSilver");

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }
}
