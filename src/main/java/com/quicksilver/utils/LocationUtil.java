package com.quicksilver.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

/**
 * Created by Derek on 12/7/2014.
 * Time: 12:40 AM
 */
public class LocationUtil {

    public static Location getNextKitSelectorLocation(World world) {
        int x = 0;
        int y = 0;
        int z = 0;

        while (new Location(world, x, y, z).getBlock().getType() == Material.BEDROCK) {
            x++;
        }
        Location location = new Location(world, x, y, z);
        location.getBlock().setType(Material.BEDROCK);
        return location.multiply(11).add(0, 40, 0);
    }
}
