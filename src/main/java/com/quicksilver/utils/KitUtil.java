package com.quicksilver.utils;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerEntityEquipment;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntityLiving;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.quicksilver.Kit;
import com.quicksilver.user.data.UserDataPaths;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by Derek on 12/3/2014.
 * Time: 6:47 PM
 */
public class KitUtil {

    private static final ArrayList<String> armorTypes = new ArrayList<>(Arrays.asList(
            UserDataPaths.SWORD,
            UserDataPaths.HELMET,
            UserDataPaths.CHESTPLATE,
            UserDataPaths.LEGGINGS,
            UserDataPaths.BOOTS
    ));
    private static int entityId = Short.MAX_VALUE;

    public static Kit getBaseKitFromConfigurationSection(ConfigurationSection configurationSection) {
        return getKitFromSection(configurationSection.getConfigurationSection(UserDataPaths.BASE_KIT), "BASE", true);
    }

    private static Kit getKitFromSection(ConfigurationSection configurationSection, String kitUUID, boolean base) {
        ArrayList<ItemStack> kitItems = new ArrayList<>();
        for (String armorType : armorTypes) {
            kitItems.add(ItemUtil.getItemStack(configurationSection.getConfigurationSection(armorType)));
        }
        return (new Kit(
                UUID.fromString(configurationSection.getString(UserDataPaths.UUID)),
                kitItems.get(0),
                kitItems.get(1),
                kitItems.get(2),
                kitItems.get(3),
                kitItems.get(4)
        ));
    }

    public static ArrayList<Kit> getKitsFromConfigurationSection(ConfigurationSection configurationSection) {
        ArrayList<Kit> kits = new ArrayList<>();
        if (configurationSection.getConfigurationSection(UserDataPaths.KITS) != null) {
            for (String kitString : configurationSection.getConfigurationSection(UserDataPaths.KITS).getKeys(false)) {
                kits.add(getKitFromSection(configurationSection, kitString, false));
            }
        }
        return kits;
    }

    public static void removeEntities(Player player, ArrayList<Integer> entities) {
        WrapperPlayServerEntityDestroy wrapperPlayServerEntityDestroy = new WrapperPlayServerEntityDestroy();

        wrapperPlayServerEntityDestroy.setEntities(entities);

        wrapperPlayServerEntityDestroy.sendPacket(player);
    }

    public static void saveBaseKitToConfig(ConfigurationSection configurationSection, Kit kit) {
        configurationSection.set(UserDataPaths.BASE_KIT, null);
        ArrayList<ItemStack> kitItems = new ArrayList<>(Arrays.asList(
                kit.getSword(),
                kit.getHelmet(),
                kit.getChestplate(),
                kit.getLeggings(),
                kit.getBoots()
        ));
        int index = 0;
        configurationSection.set(UserDataPaths.UUID, kit.getKitUUID().toString());
        for (String armorType : armorTypes) {
            ItemStack itemStack = kitItems.get(index);
            if (configurationSection.getConfigurationSection(armorType) == null) {
                configurationSection.createSection(armorType);
            }
            ItemUtil.setItemStack(itemStack, configurationSection.getConfigurationSection(armorType));
            index++;
        }
    }

    public static void saveKitsToConfig(ConfigurationSection configurationSection, ArrayList<Kit> kits) {
        configurationSection.set(configurationSection.getCurrentPath(), null);
        for (Kit kit : kits) {
            ArrayList<ItemStack> kitItems = new ArrayList<>(Arrays.asList(
                    kit.getSword(),
                    kit.getHelmet(),
                    kit.getChestplate(),
                    kit.getLeggings(),
                    kit.getBoots()
            ));
            configurationSection.createSection(kit.getKitUUID().toString());
            ConfigurationSection kitSection = configurationSection.getConfigurationSection(kit.getKitUUID().toString());
            kitSection.set(UserDataPaths.UUID, kit.getKitUUID().toString());
            int index = 0;
            for (String armorType : armorTypes) {
                ItemStack itemStack = kitItems.get(index);
                if (kitSection.getConfigurationSection(armorType) == null) {
                    kitSection.createSection(armorType);
                }
                ItemUtil.setItemStack(itemStack, kitSection.getConfigurationSection(armorType));
                index++;
            }
        }
    }

    public static int showKit(Player player, Kit kit, Location location, int pitch) {
        WrapperPlayServerSpawnEntityLiving zombie = new WrapperPlayServerSpawnEntityLiving();
        WrappedDataWatcher zombieMetaData = new WrappedDataWatcher();

        zombie.setType(EntityType.ZOMBIE);
        zombie.setMetadata(zombieMetaData);
        zombie.setEntityID(entityId++);
        zombie.setX(location.getX());
        zombie.setY(location.getY());
        zombie.setZ(location.getZ());
        zombie.setHeadYaw(pitch);

        WrapperPlayServerEntityEquipment heldItem = new WrapperPlayServerEntityEquipment();

        zombie.sendPacket(player);
        heldItem.setItem(kit.getSword());
        heldItem.setSlot((short) 0);
        heldItem.setEntityId(zombie.getEntityID());

        heldItem.sendPacket(player);

        ArrayList<ItemStack> armor = new ArrayList<>(Arrays.asList(
                kit.getBoots(),
                kit.getLeggings(),
                kit.getChestplate(),
                kit.getHelmet()));
        short slot = 1;
        for (ItemStack itemStack : armor) {
            WrapperPlayServerEntityEquipment entityEquipment = new WrapperPlayServerEntityEquipment();

            entityEquipment.setItem(itemStack);
            entityEquipment.setSlot(slot);
            entityEquipment.setEntityId(zombie.getEntityID());
            entityEquipment.sendPacket(player);
            slot++;
        }
        return zombie.getEntityID();
    }
}
