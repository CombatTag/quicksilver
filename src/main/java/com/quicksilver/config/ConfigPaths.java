package com.quicksilver.config;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by Derek on 12/1/2014.
 * Time: 11:46 PM
 */
public abstract class ConfigPaths {

    public static final String SHOP = "shop";
    public static final String STAT_WORLD = "statWorld";

    public static final String VERSION = getPath("version");

    private static String getPath(String... strings) {
        StringBuilder stringBuilder = new StringBuilder();

        Iterator iterator = Arrays.asList(strings).iterator();
        while (iterator.hasNext()) {
            stringBuilder.append(iterator.next());
            if (iterator.hasNext()) {
                stringBuilder.append(".");
            }
        }
        return stringBuilder.toString();
    }
}
