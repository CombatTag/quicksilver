package com.quicksilver.config.versions;

import com.quicksilver.config.ConfigPaths;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by Derek on 12/1/2014.
 * Time: 11:47 PM
 */
public abstract class Config0_1 {

    public static boolean equals(FileConfiguration fileConfiguration) {
        if (fileConfiguration.contains(ConfigPaths.VERSION)) {
            if (fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1")) {
                return true;
            }
        }
        return false;
    }
}
