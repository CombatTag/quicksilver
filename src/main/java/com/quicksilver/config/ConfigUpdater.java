package com.quicksilver.config;

import com.quicksilver.config.versions.Config0_1;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by Derek on 12/1/2014.
 * Time: 11:47 PM
 */
public class ConfigUpdater {

    private final FileConfiguration fileConfiguration;

    public ConfigUpdater(FileConfiguration fileConfiguration) {
        this.fileConfiguration = fileConfiguration;
    }

    public FileConfiguration update() {
        if (Config0_1.equals(fileConfiguration)) {
        }
        return fileConfiguration;
    }
}
