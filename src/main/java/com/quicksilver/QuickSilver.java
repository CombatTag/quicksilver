package com.quicksilver;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.quicksilver.commands.KitAdd;
import com.quicksilver.commands.KitUpgrade;
import com.quicksilver.commands.Shop;
import com.quicksilver.config.ConfigPaths;
import com.quicksilver.config.ConfigUpdater;
import com.quicksilver.listeners.InventoryListener;
import com.quicksilver.listeners.PlayerListener;
import com.quicksilver.listeners.WeatherListener;
import com.quicksilver.shop.ShopManager;
import com.quicksilver.user.User;
import com.quicksilver.user.UserManager;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Derek on 12/1/2014.
 * Time: 11:41 PM
 */
public class QuickSilver extends JavaPlugin {

    public static QuickSilver p;
    private final HashMap<Player, HashMap<Integer, Kit>> entities = new HashMap<>();
    private FileConfiguration fileConfiguration;
    private FileConfiguration messages;
    private Permission permission;
    private ProtocolManager protocolManager;
    private ShopManager shopManager;
    private World statWorld;
    private UserManager userManager;

    public void addEntity(Player player, int entityID, Kit kit) {
        HashMap<Integer, Kit> hashMap = new HashMap<>();
        if (entities.containsKey(player)) {
            hashMap = entities.get(player);
        }
        hashMap.put(entityID, kit);

        entities.put(player, hashMap);
    }

    public HashMap<Integer, Kit> getEntities(Player player) {
        if (entities.containsKey(player)) {
            return entities.get(player);
        }
        return null;
    }

    public FileConfiguration getFileConfiguration() {
        return fileConfiguration;
    }

    public FileConfiguration getMessages() {
        return messages;
    }

    public Permission getPermission() {
        return permission;
    }

    public ShopManager getShopManager() {
        return shopManager;
    }

    public World getStatWorld() {
        return statWorld;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    @Override
    public void onDisable() {
        userManager.unloadUsers();
    }

    @Override
    public void onEnable() {
        reload();
    }

    @Override
    public void onLoad() {
        protocolManager = ProtocolLibrary.getProtocolManager();
    }

    private void registerKitListener() {
        final Thread mainThread = Thread.currentThread();
        protocolManager.getAsynchronousManager().registerAsyncHandler(
                new PacketAdapter(p, ListenerPriority.NORMAL, PacketType.Play.Client.USE_ENTITY) {
                    @Override
                    public void onPacketReceiving(PacketEvent event) {
                        if (event.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
                            try {
                                if (Thread.currentThread().getId() == mainThread.getId()) {
                                    PacketContainer packet = event.getPacket();

                                    final Player player = event.getPlayer();

                                    if (entities.containsKey(player)) {
                                        final HashMap<Integer, Kit> players = entities.get(player);
                                        final int entityID = packet.getIntegers().read(0);

                                        if (!event.isCancelled()) {
                                            event.getAsyncMarker().incrementProcessingDelay();

                                            getServer().getScheduler().scheduleSyncDelayedTask(QuickSilver.p, new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (players.containsKey(entityID)) {
                                                        Kit kit = players.get(entityID);
                                                        player.getInventory().addItem(
                                                                kit.getSword(),
                                                                kit.getHelmet(),
                                                                kit.getChestplate(),
                                                                kit.getLeggings(),
                                                                kit.getBoots()
                                                        );
                                                        User user = userManager.getUser(player);

                                                        user.removeKit(kit);
                                                    }
                                                }
                                            }, 0);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        ).syncStart();
    }

    public void reload() {
        p = this;

        reloadFileConfiguration();
        reloadMessages();
        reloadListeners();
        reloadWorlds();
        reloadTpCharger();
        reloadDependencies();

        PluginCommand command = p.getCommand("kitadd");
        command.setExecutor(new KitAdd());

        PluginCommand command2 = p.getCommand("kitupgrade");
        command2.setExecutor(new KitUpgrade());

        PluginCommand command3 = p.getCommand("shop");
        command3.setExecutor(new Shop());
    }

    private void reloadDependencies() {
        setPermissions();
    }

    private void reloadFileConfiguration() {
        if (!FilePaths.USER_DATA_FOLDER.exists()) {
            FilePaths.USER_DATA_FOLDER.mkdirs();
        }

        fileConfiguration = new ConfigUpdater(getConfig()).update();
        getConfig().options().copyDefaults(true);
        saveConfig();
        fileConfiguration = getConfig();

        userManager = new UserManager();
        shopManager = new ShopManager(fileConfiguration.getConfigurationSection(ConfigPaths.SHOP));
    }

    private void reloadListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        HandlerList.unregisterAll(this);

        final InventoryListener inventoryListener = new InventoryListener();
        final PlayerListener playerListener = new PlayerListener();
        final WeatherListener weatherListener = new WeatherListener();

        pluginManager.registerEvents(inventoryListener, this);
        pluginManager.registerEvents(playerListener, this);
        pluginManager.registerEvents(weatherListener, this);
        pluginManager.registerEvents(shopManager, this);

        registerKitListener();
    }

    private void reloadMessages() {
        try {
            Reader messagesStream = new InputStreamReader(this.getResource("messages.yml"), "UTF8");
            messages = YamlConfiguration.loadConfiguration(messagesStream);
        } catch (UnsupportedEncodingException e) {
            System.out.println("An error has occurred while loading the plugin messages.");
        }
    }

    private void reloadTpCharger() {
        for (Player player : getServer().getOnlinePlayers()) {
            User user = userManager.getUser(player);

            user.cancelNextCharge();
            user.scheduleNextCharge();
        }
    }

    private void reloadWorlds() {
        String worldName = fileConfiguration.getString(ConfigPaths.STAT_WORLD);
        if (Bukkit.getWorld(worldName) == null) {
            Bukkit.createWorld(new WorldCreator(worldName).generator(new VoidGenerator()));
        }
        statWorld = Bukkit.getWorld(worldName);
    }

    private void setPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
    }
}
