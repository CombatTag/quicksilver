package com.quicksilver.user;

import com.quicksilver.Kit;
import com.quicksilver.QuickSilver;
import com.quicksilver.utils.KitUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.UUID;

/**
 * Created by Derek on 12/3/2014.
 * Time: 6:45 PM
 */
public class User {

    private final UserManager parent;
    private double balance;
    private Kit baseKit;
    private int charges;
    private Location kitSelectorLocation;
    private ArrayList<Kit> kits;
    private long lastCharge;
    private long lastKit;
    private long lastLogin;
    private String lastUsername;
    private PlayerDamage playerDamage;
    private UUID playerUUID;
    private int taskID;
    private double worth;

    public User(UserManager userManager, String lastUsername, UUID playerUUID, long lastLogin, long lastKit, ArrayList<Kit> kits, Kit baseKit, Location kitSelectorLocation, double balance, double worth, int charges, long lastCharge) {
        this.parent = userManager;
        this.lastUsername = lastUsername;
        this.playerUUID = playerUUID;
        this.lastLogin = lastLogin;
        this.lastKit = lastKit;
        this.kits = kits;
        this.baseKit = baseKit;
        this.kitSelectorLocation = kitSelectorLocation;
        this.balance = balance;
        this.worth = worth;
        this.charges = charges;
        this.lastCharge = lastCharge;

        taskID = -1;
        playerDamage = new PlayerDamage();
    }

    public void addKit(Kit... kit) {
        Player player = Bukkit.getPlayer(getLastUsername());
        if (QuickSilver.p.getEntities(player) != null) {
            KitUtil.removeEntities(player, new ArrayList<>(QuickSilver.p.getEntities(player).keySet()));
        }
        int previousKitSelectorLength = kits.size();
        kits.addAll(Arrays.asList(kit));
        updateKitSelector(previousKitSelectorLength);
    }

    public void cancelNextCharge() {
        if (taskID != -1) {
            Bukkit.getScheduler().cancelTask(taskID);
            taskID = -1;
        }
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Kit getBaseKit() {
        return baseKit;
    }

    public void setBaseKit(Kit baseKit) {
        this.baseKit = baseKit;
    }

    public int getCharges() {
        return charges;
    }

    public void setCharges(int charges) {
        this.charges = charges;
    }

    public Location getKitSelectorLocation() {
        return kitSelectorLocation.clone();
    }

    public void setKitSelectorLocation(Location kitSelectorLocation) {
        this.kitSelectorLocation = kitSelectorLocation;
    }

    public ArrayList<Kit> getKits() {
        return kits;
    }

    public void setKits(ArrayList<Kit> kits) {
        this.kits = kits;
    }

    public long getLastCharge() {
        return lastCharge;
    }

    public void setLastCharge(long lastCharge) {
        this.lastCharge = lastCharge;
    }

    public long getLastKit() {
        return lastKit;
    }

    public void setLastKit(long lastKit) {
        this.lastKit = lastKit;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getLastUsername() {
        return lastUsername;
    }

    public void setLastUsername(String lastUsername) {
        this.lastUsername = lastUsername;
    }

    public Kit getNewKit() {
        return new Kit(UUID.randomUUID(), baseKit.getSword(), baseKit.getHelmet(), baseKit.getChestplate(), baseKit.getLeggings(), baseKit.getBoots());
    }

    private long getNextChargeInTicks() {
        long totalTimeTicks = 72000;
        long timePastedTicks = (System.currentTimeMillis() - getLastKit()) / 50;

        long tickDiff = totalTimeTicks - timePastedTicks;

        return tickDiff < -1 ? 0 : tickDiff;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(lastUsername);
    }

    public PlayerDamage getPlayerDamage() {
        return playerDamage;
    }

    public void setPlayerDamage(PlayerDamage playerDamage) {
        this.playerDamage = playerDamage;
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }

    public void setPlayerUUID(UUID playerUUID) {
        this.playerUUID = playerUUID;
    }

    public double getWorth() {
        return worth;
    }

    public void setWorth(double worth) {
        this.worth = worth;
    }

    public boolean isOnline() {
        Player player = Bukkit.getPlayer(lastUsername);

        if (player != null) {
            if (player.isOnline()) {
                return true;
            }
        }
        return false;
    }

    public void removeKit(Kit... kits) {
        Player player = Bukkit.getPlayer(getLastUsername());
        if (QuickSilver.p.getEntities(player) != null) {
            KitUtil.removeEntities(player, new ArrayList<>(QuickSilver.p.getEntities(player).keySet()));
        }
        int previousKitSelectorLength = this.kits.size();
        int i = 0;
        while (i < kits.length) {
            Kit kit = kits[i];
            int i2 = 0;
            while (i2 < this.kits.size()) {
                Kit kit2 = this.kits.get(i2);
                if (kit.getKitUUID().toString().equals(kit2.getKitUUID().toString())) {
                    this.kits.remove(i2);
                }
                i2++;
            }
            i++;
        }
        updateKitSelector(previousKitSelectorLength);
    }

    public void scheduleNextCharge() {
        Bukkit.getScheduler().scheduleSyncDelayedTask(QuickSilver.p, new Runnable() {
            @Override
            public void run() {
                if (charges < 3) {
                    charges++;
                }
            }
        }, getNextChargeInTicks());
    }

    public void sendMessage(String message) {
        Player player = Bukkit.getPlayer(lastUsername);

        if (player != null) {
            player.sendMessage(message);
        }
    }

    public void unload() {
        cancelNextCharge();
        parent.unloadUser(this);
    }

    public void updateKitSelector(int previousKitSelectorLength) {
        Iterator<Kit> iterator = kits.iterator();
        if (previousKitSelectorLength <= 4) {
            previousKitSelectorLength = 9;
        } else {
            previousKitSelectorLength = kits.size();
            if (previousKitSelectorLength % 2 != 0) {
                previousKitSelectorLength += 1;
            }
            previousKitSelectorLength /= 2;
            previousKitSelectorLength *= 4;
            previousKitSelectorLength += 5;
        }
        for (int zOffset = 0; zOffset < previousKitSelectorLength; zOffset++) {
            for (int xOffset = -5; xOffset <= 5; xOffset++) {
                for (int yOffset = -1; yOffset <= 4; yOffset++) {
                    Location location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() + xOffset, getKitSelectorLocation().getY() + yOffset, getKitSelectorLocation().getZ() + zOffset);

                    location.getBlock().setType(Material.AIR);
                }
            }
        }
        int kitSelectorLength;
        if (kits.size() <= 4) {
            kitSelectorLength = 9;
        } else {
            kitSelectorLength = kits.size();
            if (kitSelectorLength % 2 != 0) {
                kitSelectorLength += 1;
            }
            kitSelectorLength /= 2;
            kitSelectorLength *= 4;
            kitSelectorLength += 1;
        }
        for (int zOffset = 0; zOffset < kitSelectorLength; zOffset++) {
            for (int xOffset = -5; xOffset <= 5; xOffset++) {
                Location location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() + xOffset, getKitSelectorLocation().getY(), getKitSelectorLocation().getZ() + zOffset);

                location.getBlock().setType(Material.SMOOTH_BRICK);

                location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() + xOffset, getKitSelectorLocation().getY() + 4, getKitSelectorLocation().getZ() + zOffset);

                location.getBlock().setType(Material.SMOOTH_BRICK);

                if (zOffset == 0 || zOffset == kitSelectorLength - 1 || xOffset == -5 || xOffset == 5) {
                    for (int yOffset = 1; yOffset <= 3; yOffset++) {
                        location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() + xOffset, getKitSelectorLocation().getY() + yOffset, getKitSelectorLocation().getZ() + zOffset);

                        location.getBlock().setType(Material.SMOOTH_BRICK);
                    }
                }
            }
        }
        Location location;

        if (Bukkit.getPlayer(getLastUsername()) != null) {
            Bukkit.getPlayer(getLastUsername()).teleport(getKitSelectorLocation().add(0.5, 1, 1.5));
        }

        int kitsLeft = kits.size();

        if (kitsLeft % 2 == 0) {
            location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() - 2, getKitSelectorLocation().getY() + 1, getKitSelectorLocation().getZ() + kitSelectorLength - 2);

            location.getBlock().setType(Material.STEP);

            if (iterator.hasNext()) {
                Kit kit = iterator.next();
                QuickSilver.p.addEntity(Bukkit.getPlayer(getLastUsername()), KitUtil.showKit(Bukkit.getPlayer(getLastUsername()), kit, location.add(0.5, 0.5, 0.5), 180), kit);
            }

            location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() + 2, getKitSelectorLocation().getY() + 1, getKitSelectorLocation().getZ() + kitSelectorLength - 2);

            location.getBlock().setType(Material.STEP);

            if (iterator.hasNext()) {
                Kit kit = iterator.next();
                QuickSilver.p.addEntity(Bukkit.getPlayer(getLastUsername()), KitUtil.showKit(Bukkit.getPlayer(getLastUsername()), kit, location.add(0.5, 0.5, 0.5), 180), kit);
            }

            kitsLeft -= 2;
        } else {
            location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX(), getKitSelectorLocation().getY() + 1, getKitSelectorLocation().getZ() + kitSelectorLength - 2);

            location.getBlock().setType(Material.STEP);

            if (iterator.hasNext()) {
                Kit kit = iterator.next();
                QuickSilver.p.addEntity(Bukkit.getPlayer(getLastUsername()), KitUtil.showKit(Bukkit.getPlayer(getLastUsername()), kit, location.add(0.5, 0.5, 0.5), 180), kit);
            }

            kitsLeft -= 1;
        }

        for (int i = 0; i < kitsLeft - 1; i += 2) {
            int zOffset = i;
            if (zOffset % 2 != 0) {
                zOffset += 1;
            }
            zOffset /= 2;
            zOffset *= 4;
            zOffset += 4;

            location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX(), getKitSelectorLocation().getY(), getKitSelectorLocation().getZ() + zOffset);

            location.getBlock().setType(Material.REDSTONE_LAMP_OFF);

            location.subtract(0, 1, 0);

            location.getBlock().setType(Material.REDSTONE_BLOCK);

            location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() - 4, getKitSelectorLocation().getY() + 1, getKitSelectorLocation().getZ() + zOffset);

            location.getBlock().setType(Material.STEP);

            if (iterator.hasNext()) {
                Kit kit = iterator.next();
                QuickSilver.p.addEntity(Bukkit.getPlayer(getLastUsername()), KitUtil.showKit(Bukkit.getPlayer(getLastUsername()), kit, location.add(0.5, 0.5, 0.5), -90), kit);
            }

            location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX() + 4, getKitSelectorLocation().getY() + 1, getKitSelectorLocation().getZ() + zOffset);

            location.getBlock().setType(Material.STEP);

            if (iterator.hasNext()) {
                Kit kit = iterator.next();
                QuickSilver.p.addEntity(Bukkit.getPlayer(getLastUsername()), KitUtil.showKit(Bukkit.getPlayer(getLastUsername()), kit, location.add(0.5, 0.5, 0.5), 90), kit);
            }
        }

        location = new Location(QuickSilver.p.getStatWorld(), getKitSelectorLocation().getX(), getKitSelectorLocation().getY(), getKitSelectorLocation().getZ() + 4);

        location.getBlock().setType(Material.REDSTONE_LAMP_OFF);

        location.subtract(0, 1, 0);

        location.getBlock().setType(Material.REDSTONE_BLOCK);
    }
}
