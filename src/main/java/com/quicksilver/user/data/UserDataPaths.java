package com.quicksilver.user.data;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by Derek on 12/3/2014.
 * Time: 6:50 PM
 */
public abstract class UserDataPaths {

    public static final String BALANCE = getPath("balance");
    public static final String BASE_KIT = getPath("baseKit");
    public static final String BOOTS = getPath("boots");
    public static final String CHARGES = getPath("charges");
    public static final String CHESTPLATE = getPath("chestplate");
    public static final String HELMET = getPath("helmet");
    public static final String KITS = getPath("kits");
    public static final String KITS_KIT = getPath(KITS, "kit");
    public static final String KITS_KIT_ARMOR_TYPE = getPath(KITS_KIT, "armorType");
    public static final String KITS_KIT_ARMOR_TYPE_AMOUNT = getPath(KITS_KIT_ARMOR_TYPE, "amount");
    public static final String KITS_KIT_ARMOR_TYPE_DISPLAY_NAME = getPath(KITS_KIT_ARMOR_TYPE, "displayName");
    public static final String KITS_KIT_ARMOR_TYPE_ENCHANTMENTS = getPath(KITS_KIT_ARMOR_TYPE, "enchantments");
    public static final String KITS_KIT_ARMOR_TYPE_ENCHANTMENTS_ENCHANT_TYPE = getPath(KITS_KIT_ARMOR_TYPE_ENCHANTMENTS, "enchantmentType");
    public static final String KITS_KIT_ARMOR_TYPE_ENCHANTMENTS_ENCHANT_TYPE_LEVEL = getPath(KITS_KIT_ARMOR_TYPE_ENCHANTMENTS_ENCHANT_TYPE, "level");
    public static final String KITS_KIT_ARMOR_TYPE_LORE = getPath(KITS_KIT_ARMOR_TYPE, "lore");
    public static final String KITS_KIT_ARMOR_TYPE_MATERIAL = getPath(KITS_KIT_ARMOR_TYPE, "material");
    public static final String KITS_KIT_UUID = getPath(KITS_KIT, "kitUUID");
    public static final String KIT_SELECTOR_LOCATION = getPath("kitSelectorLocation");
    public static final String KIT_SELECTOR_LOCATION_WORLD = getPath(KIT_SELECTOR_LOCATION, "World");
    public static final String KIT_SELECTOR_LOCATION_X = getPath(KIT_SELECTOR_LOCATION, "X");
    public static final String KIT_SELECTOR_LOCATION_Y = getPath(KIT_SELECTOR_LOCATION, "Y");
    public static final String KIT_SELECTOR_LOCATION_Z = getPath(KIT_SELECTOR_LOCATION, "Z");
    public static final String LAST_CHARGE = getPath("lastCharge");
    public static final String LAST_KIT = getPath("lastKit");
    public static final String LAST_LOGIN = getPath("lastLogin");
    public static final String LAST_USERNAME = getPath("lastUsername");
    public static final String LEGGINGS = getPath("leggings");
    public static final String SWORD = getPath("sword");
    public static final String UUID = getPath("UUID");
    public static final String WORTH = getPath("worth");

    public static String getArmorPath(String path, String kitUUID, String armorType, boolean base) {
        return getTruePath(path, kitUUID, armorType, "", base);
    }

    public static String getEnchantmentPath(String path, String kitUUID, String armorType, String enchantmentType, boolean base) {
        return getTruePath(path, kitUUID, armorType, enchantmentType, base);
    }

    public static String getKitPath(String path, String kitUUID, boolean base) {
        return getTruePath(path, kitUUID, "", "", base);
    }

    private static String getPath(String... strings) {
        StringBuilder stringBuilder = new StringBuilder();

        Iterator iterator = Arrays.asList(strings).iterator();
        while (iterator.hasNext()) {
            stringBuilder.append(iterator.next());
            if (iterator.hasNext()) {
                stringBuilder.append(".");
            }
        }
        return stringBuilder.toString();
    }

    public static String getTruePath(String path, String kitUUID, String armorType, String enchantmentType, boolean base) {
        StringBuilder stringBuilder = new StringBuilder();

        String[] splitPath = path.split("\\.");

        if (splitPath.length >= 2) {
            if (splitPath[1].equals("kit")) {
                splitPath[1] = kitUUID;
            }
        }

        if (!armorType.equals("")) {
            if (splitPath.length >= 3) {
                if (splitPath[2].equals("armorType")) {
                    splitPath[2] = armorType;
                }
            }
        }

        if (!enchantmentType.equals("")) {
            if (splitPath.length >= 5) {
                if (splitPath[3].equals("enchantments")) {
                    if (splitPath[4].equals("enchantmentType")) {
                        splitPath[4] = enchantmentType;
                    }
                }
            }
        }

        Iterator iterator = Arrays.asList(splitPath).iterator();
        while (iterator.hasNext()) {
            stringBuilder.append(iterator.next());
            if (iterator.hasNext()) {
                stringBuilder.append(".");
            }
        }

        if (base) {
            if (splitPath.length >= 2) {
                if (splitPath[1].equals(kitUUID)) {
                    splitPath[1] = "kit";
                }
            }

            stringBuilder = new StringBuilder();
            iterator = Arrays.asList(splitPath).iterator();
            while (iterator.hasNext()) {
                stringBuilder.append(iterator.next());
                if (iterator.hasNext()) {
                    stringBuilder.append(".");
                }
            }

            stringBuilder = new StringBuilder(stringBuilder.toString().replace(KITS_KIT, BASE_KIT));
        }
        return stringBuilder.toString();
    }
}
