package com.quicksilver.user.data;

import com.quicksilver.FilePaths;
import com.quicksilver.Kit;
import com.quicksilver.QuickSilver;
import com.quicksilver.user.User;
import com.quicksilver.user.UserManager;
import com.quicksilver.utils.KitUtil;
import com.quicksilver.utils.LocationUtil;
import com.quicksilver.utils.TierUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Derek on 12/3/2014.
 * Time: 7:39 PM
 */
public class UserDataManager {

    private final UserManager parent;

    public UserDataManager(UserManager userManager) {
        this.parent = userManager;
    }

    public User getUserData(UUID playerUUID) {
        String lastUsername = "";
        long lastLogin = 0;
        long lastKit = 0;
        ArrayList<Kit> kits = new ArrayList<>();
        Kit baseKit = TierUtil.getDefaultKit();
        Location kitSelectorLocation = null;
        double balance = 0;
        double worth = 0;
        int charges = 3;
        long lastCharge = 0;

        File userFile = new File(FilePaths.USER_DATA_FOLDER, playerUUID.toString() + ".yml");

        if (userFile.exists()) {
            FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(userFile);

            lastUsername = fileConfiguration.getString(UserDataPaths.LAST_USERNAME);
            lastLogin = fileConfiguration.getLong(UserDataPaths.LAST_LOGIN);
            lastKit = fileConfiguration.getLong(UserDataPaths.LAST_KIT);
            kits = KitUtil.getKitsFromConfigurationSection(fileConfiguration.getConfigurationSection(UserDataPaths.KITS));
            baseKit = KitUtil.getBaseKitFromConfigurationSection(fileConfiguration.getConfigurationSection(UserDataPaths.KITS));
            if (Bukkit.getWorld(fileConfiguration.getString(UserDataPaths.KIT_SELECTOR_LOCATION_WORLD)) != null) {
                kitSelectorLocation = new Location(
                        Bukkit.getWorld(fileConfiguration.getString(UserDataPaths.KIT_SELECTOR_LOCATION_WORLD)),
                        fileConfiguration.getInt(UserDataPaths.KIT_SELECTOR_LOCATION_X),
                        fileConfiguration.getInt(UserDataPaths.KIT_SELECTOR_LOCATION_Y),
                        fileConfiguration.getInt(UserDataPaths.KIT_SELECTOR_LOCATION_Z)
                );
            }
            balance = fileConfiguration.getDouble(UserDataPaths.BALANCE);
            worth = fileConfiguration.getDouble(UserDataPaths.WORTH);
            charges = fileConfiguration.getInt(UserDataPaths.CHARGES);
            lastCharge = fileConfiguration.getLong(UserDataPaths.LAST_CHARGE);
        } else {
            kitSelectorLocation = LocationUtil.getNextKitSelectorLocation(QuickSilver.p.getStatWorld());
        }
        return new User(parent, lastUsername, playerUUID, lastLogin, lastKit, kits, baseKit, kitSelectorLocation, balance, worth, charges, lastCharge);
    }

    public void saveUserData(User user) {
        File userFile = new File(FilePaths.USER_DATA_FOLDER, user.getPlayerUUID().toString() + ".yml");

        try {
            FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(userFile);

            fileConfiguration.set(UserDataPaths.BALANCE, user.getBalance());
            fileConfiguration.set(UserDataPaths.CHARGES, user.getCharges());
            fileConfiguration.set(UserDataPaths.KIT_SELECTOR_LOCATION_WORLD, user.getKitSelectorLocation().getWorld().getName());
            fileConfiguration.set(UserDataPaths.KIT_SELECTOR_LOCATION_X, user.getKitSelectorLocation().getX());
            fileConfiguration.set(UserDataPaths.KIT_SELECTOR_LOCATION_Y, user.getKitSelectorLocation().getY());
            fileConfiguration.set(UserDataPaths.KIT_SELECTOR_LOCATION_Z, user.getKitSelectorLocation().getZ());
            fileConfiguration.set(UserDataPaths.LAST_CHARGE, user.getLastCharge());
            fileConfiguration.set(UserDataPaths.LAST_USERNAME, user.getLastUsername());
            fileConfiguration.set(UserDataPaths.LAST_LOGIN, user.getLastLogin());
            fileConfiguration.set(UserDataPaths.LAST_KIT, user.getLastKit());
            fileConfiguration.set(UserDataPaths.WORTH, user.getWorth());
            if (fileConfiguration.getConfigurationSection(UserDataPaths.KITS) == null) {
                fileConfiguration.createSection(UserDataPaths.KITS);
            }
            if (fileConfiguration.getConfigurationSection(UserDataPaths.KITS).getConfigurationSection(UserDataPaths.BASE_KIT) == null) {
                fileConfiguration.getConfigurationSection(UserDataPaths.KITS).createSection(UserDataPaths.BASE_KIT);
            }
            KitUtil.saveKitsToConfig(fileConfiguration.getConfigurationSection(UserDataPaths.KITS), user.getKits());
            KitUtil.saveBaseKitToConfig(fileConfiguration.getConfigurationSection(UserDataPaths.KITS).getConfigurationSection(UserDataPaths.BASE_KIT), user.getBaseKit());
            fileConfiguration.save(userFile);
        } catch (IOException exception) {
            System.out.println("A IOException has occurred, player config could not be loaded.");
        }
    }
}
