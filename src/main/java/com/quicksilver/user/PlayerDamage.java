package com.quicksilver.user;

import com.quicksilver.QuickSilver;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Derek on 12/24/2014.
 * Time: 11:12 PM
 */
public class PlayerDamage {

    private final HashMap<Player, Double> damagers = new HashMap<>();

    public void damage(final Player damager, final Double damage) {
        damagers.put(damager, damage + getDamage(damager));

        if (damage > 0) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(QuickSilver.p, new Runnable() {
                @Override
                public void run() {
                    damage(damager, damage * -1);
                }
            }, 600);
        }
    }

    private Double getDamage(Player player) {
        double damage = 0;

        if (damagers.containsKey(player)) {
            damage = damagers.get(player);
        }

        return damage;
    }

    public HashMap<Player, Double> getDamagers() {
        HashMap<Player, Double> damagers = new HashMap<>();
        double totalDamage = getTotalDamage();

        for (Map.Entry<Player, Double> entry : this.damagers.entrySet()) {
            damagers.put(entry.getKey(), entry.getValue() / totalDamage);
        }

        return damagers;
    }

    private Double getTotalDamage() {
        double damage = 0;
        for (Double currentDamage : damagers.values()) {
            damage += currentDamage;
        }
        return damage;
    }
}
