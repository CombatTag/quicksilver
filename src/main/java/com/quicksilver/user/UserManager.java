package com.quicksilver.user;

import com.quicksilver.FilePaths;
import com.quicksilver.QuickSilver;
import com.quicksilver.user.data.UserDataManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Derek on 12/24/2014.
 * Time: 11:29 PM
 */
public class UserManager {

    private final UserDataManager userDataManager;
    private final ConcurrentHashMap<UUID, User> userMap = new ConcurrentHashMap<>();

    public UserManager() {
        this.userDataManager = new UserDataManager(this);
    }

    public User getUser(final UUID playerUUID) {
        if (!userMap.containsKey(playerUUID)) {
            userMap.put(playerUUID, userDataManager.getUserData(playerUUID));
            Bukkit.getScheduler().scheduleSyncRepeatingTask(QuickSilver.p, new Runnable() {
                @Override
                public void run() {
                    UserManager userManager = QuickSilver.p.getUserManager();
                    userManager.saveUser(userManager.getUser(playerUUID));
                }
            }, 6000, 6000);
        }
        return userMap.get(playerUUID);
    }

    public User getUser(Player player) {
        return getUser(player.getUniqueId());
    }

    public User getUser(HumanEntity humanEntity) {
        return getUser(humanEntity.getUniqueId());
    }

    public User getUser(String name) {
        Player player = Bukkit.getPlayer(name);

        if (player == null) {
            File[] userDataFiles = FilePaths.USER_DATA_FOLDER.listFiles();

            for (File file : userDataFiles != null ? userDataFiles : new File[0]) {
                User userData = getUser(UUID.fromString(file.getName().replace(".yml", "")));
                if (userData.getLastUsername().equalsIgnoreCase(name)) {
                    return userData;
                }
            }
        } else {
            return getUser(player);
        }
        return null;
    }

    public void saveUser(User user) {
        userDataManager.saveUserData(user);
    }

    public void unloadUser(User user) {
        saveUser(user);
        userMap.remove(user.getPlayerUUID());
    }

    public void unloadUsers() {
        for (User user : userMap.values()) {
            unloadUser(user);
        }
    }
}
