package com.quicksilver.listeners;

import com.quicksilver.QuickSilver;
import com.quicksilver.user.User;
import com.quicksilver.utils.BankUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Map;

/**
 * Created by Derek on 12/3/2014.
 * Time: 6:41 PM
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            User user = QuickSilver.p.getUserManager().getUser((Player) event.getEntity());

            user.getPlayerDamage().damage((Player) event.getDamager(), event.getDamage());
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        User user = QuickSilver.p.getUserManager().getUser(event.getEntity());

        if (user.getWorth() >= 1) {
            for (Map.Entry<Player, Double> entry : user.getPlayerDamage().getDamagers().entrySet()) {
                if (entry.getKey().isOnline()) {
                    Player player = entry.getKey();

                    ItemStack quickSilver = new ItemStack(Material.GHAST_TEAR, (int) Math.floor(user.getWorth() * entry.getValue()));
                    ItemMeta itemMeta = quickSilver.getItemMeta();

                    itemMeta.setDisplayName(ChatColor.BLUE + "QuickSilver");

                    quickSilver.setItemMeta(itemMeta);

                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), quickSilver);
                }
            }
        }

        double worth = user.getWorth() * 0.3;

        if (worth < 1) {
            worth = 1;
        }

        user.setWorth(0);

        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        Objective objective = scoreboard.getObjective(DisplaySlot.BELOW_NAME);

        if (objective == null) {
            objective = scoreboard.registerNewObjective("quicksilver", "dummy");
        }

        objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
        objective.setDisplayName("QuickSilver ");

        Score score = objective.getScore(user.getLastUsername());

        score.setScore((int) Math.floor(user.getWorth()));

        if (event.getEntity().getKiller() != null) {
            User killer = QuickSilver.p.getUserManager().getUser(event.getEntity().getKiller());

            killer.setWorth(killer.getWorth() + worth);

            score = objective.getScore(killer.getLastUsername());

            score.setScore((int) Math.floor(killer.getWorth()));

            Bukkit.getPlayer(killer.getLastUsername()).setScoreboard(scoreboard);
        }
        Bukkit.getPlayer(user.getLastUsername()).setScoreboard(scoreboard);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();
            if (block.getLocation().getBlockX() == 160 && block.getLocation().getBlockY() == 63 && block.getLocation().getBlockZ() == 62) {
                if (block.getType() == Material.ENDER_CHEST) {
                    Inventory inventory = Bukkit.createInventory(null, 9, "Bank");

                    inventory.setItem(0, BankUtil.getBankItem(QuickSilver.p.getUserManager().getUser(event.getPlayer()).getBalance()));

                    event.getPlayer().openInventory(inventory);

                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        User user = QuickSilver.p.getUserManager().getUser(event.getPlayer());

        user.setLastUsername(event.getPlayer().getName());
        user.setLastLogin(System.currentTimeMillis());

        if (System.currentTimeMillis() - user.getLastKit() >= 6000000) {
            user.addKit(
                    user.getNewKit(),
                    user.getNewKit(),
                    user.getNewKit(),
                    user.getNewKit(),
                    user.getNewKit()
            );
            user.setLastKit(System.currentTimeMillis());
        }

        user.scheduleNextCharge();
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        QuickSilver.p.getUserManager().getUser(event.getPlayer()).unload();
    }
}
