package com.quicksilver.listeners;

import com.quicksilver.QuickSilver;
import com.quicksilver.utils.BankUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import java.util.ArrayList;

import static com.quicksilver.utils.ChatUtil.t;

/**
 * Created by Derek on 12/13/2014.
 * Time: 2:21 AM
 */
public class InventoryListener implements Listener {

    private final ArrayList<Player> players = new ArrayList<>();

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.getClickedInventory() != null) {
            if (event.getClickedInventory().getName().equalsIgnoreCase("Bank")) {
                if (event.getAction() == InventoryAction.PLACE_SOME ||
                        event.getAction() == InventoryAction.PLACE_ALL ||
                        event.getAction() == InventoryAction.PLACE_ONE) {
                    BankUtil.depositQuickSilver(event.getClickedInventory(), event.getWhoClicked(), event.getSlot());
                } else {
                    event.setCancelled(true);
                }
                if (event.getCurrentItem().equals(BankUtil.getBankItem(event.getWhoClicked()))) {
                    if (event.getAction() == InventoryAction.PICKUP_ALL ||
                            event.getAction() == InventoryAction.PICKUP_HALF) {
                        final Player player = Bukkit.getPlayer(event.getWhoClicked().getName());
                        if (player != null) {
                            if (!players.contains(player)) {
                                event.getWhoClicked().closeInventory();
                                player.sendMessage(t("withdrawAmount"));
                                players.add(player);
                                final InputListener inputListener = new InputListener(player, event.getAction() == InventoryAction.PICKUP_HALF, this);
                                QuickSilver.p.getServer().getPluginManager().registerEvents(inputListener, QuickSilver.p);
                            }
                        }
                    }
                }
            } else {
                if (event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
                    if (event.getInventory().getName().equals("Bank")) {
                        Integer[] slots = new Integer[event.getInventory().getSize()];
                        for (int slot = 0; slot < event.getInventory().getSize(); slot++) {
                            slots[slot] = slot;
                        }
                        BankUtil.depositQuickSilver(event.getInventory(), event.getWhoClicked(), slots);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (event.getInventory() != null) {
            if (event.getInventory().getName().equals("Bank")) {
                BankUtil.depositQuickSilver(event.getInventory(), event.getWhoClicked(), event.getInventorySlots().toArray(new Integer[event.getInventorySlots().size()]));
            }
        }
    }

    public void removePlayer(Player player) {
        players.remove(player);
    }
}