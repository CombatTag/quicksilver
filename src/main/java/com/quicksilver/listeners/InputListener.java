package com.quicksilver.listeners;

import com.quicksilver.QuickSilver;
import com.quicksilver.user.User;
import com.quicksilver.utils.BankUtil;
import com.quicksilver.utils.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;

import static com.quicksilver.utils.ChatUtil.t;

/**
 * Created by Derek on 12/13/2014.
 * Time: 7:25 PM
 */
public class InputListener implements Listener {

    private final boolean banknote;
    private final InventoryListener inventoryListener;
    final private Player player;
    private boolean canceled;

    public InputListener(final Player player, boolean banknote, final InventoryListener inventoryListener) {
        this.player = player;
        this.banknote = banknote;
        final Listener listener = this;
        this.inventoryListener = inventoryListener;
        Bukkit.getScheduler().scheduleSyncDelayedTask(QuickSilver.p, new Runnable() {
            @Override
            public void run() {
                if (!canceled) {
                    HandlerList.unregisterAll(listener);
                    player.sendMessage(t("noLongerListening"));
                    inventoryListener.removePlayer(player);
                }
            }
        }, 300);
    }

    public Player getPlayer() {
        return player;
    }

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (event.getPlayer() == player) {
            try {
                Integer integer = Integer.valueOf(event.getMessage());
                User user = QuickSilver.p.getUserManager().getUser(player);
                if (user != null) {
                    event.setCancelled(true);
                    if (user.getBalance() < integer) {
                        user.sendMessage(t("notEnough", String.valueOf((int) Math.floor(user.getBalance())), String.valueOf(integer)));
                        return;
                    }
                    if (integer < 1) {
                        user.sendMessage(t("notValidNumber", String.valueOf(integer)));
                        return;
                    }
                    int availableAmount = ItemUtil.getAvailableAmount(event.getPlayer().getInventory(), BankUtil.getQuickSilver(1));
                    if (availableAmount < integer) {
                        user.sendMessage(t("notEnoughSpace", String.valueOf(availableAmount), String.valueOf(integer)));
                        return;
                    }
                    ItemStack itemStack = banknote ? BankUtil.getBankNote(integer, 1) : BankUtil.getQuickSilver(integer);

                    user.setBalance(user.getBalance() - integer);
                    user.sendMessage(t("withdrawn", String.valueOf(integer)));

                    HandlerList.unregisterAll(this);
                    inventoryListener.removePlayer(player);
                    canceled = true;

                    event.getPlayer().getInventory().addItem(itemStack);
                }
            } catch (NumberFormatException e) {
                event.getPlayer().sendMessage(t("notValidNumber", event.getMessage()));
            }
        }
    }
}
