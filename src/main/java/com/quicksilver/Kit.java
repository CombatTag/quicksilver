package com.quicksilver;

import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Derek on 12/1/2014.
 * Time: 11:58 PM
 */
public class Kit {

    private ItemStack boots;
    private ItemStack chestplate;
    private ItemStack helmet;
    private UUID kitUUID;
    private ItemStack leggings;
    private ItemStack sword;

    public Kit(UUID kitUUID, ItemStack sword, ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots) {
        this.kitUUID = kitUUID;
        this.sword = sword;
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggings = leggings;
        this.boots = boots;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public void setBoots(ItemStack boots) {
        this.boots = boots;
    }

    public ItemStack getChestplate() {
        return chestplate;
    }

    public void setChestplate(ItemStack chestplate) {
        this.chestplate = chestplate;
    }

    public ItemStack getHelmet() {
        return helmet;
    }

    public void setHelmet(ItemStack helmet) {
        this.helmet = helmet;
    }

    public UUID getKitUUID() {
        return kitUUID;
    }

    public void setKitUUID(UUID kitUUID) {
        this.kitUUID = kitUUID;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public void setLeggings(ItemStack leggings) {
        this.leggings = leggings;
    }

    public ItemStack getSword() {
        return sword;
    }

    public void setSword(ItemStack sword) {
        this.sword = sword;
    }
}
