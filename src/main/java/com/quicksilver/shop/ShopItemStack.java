package com.quicksilver.shop;

import org.bukkit.inventory.ItemStack;

/**
 * Created by Derek on 12/25/2014.
 * Time: 10:00 PM
 */
public class ShopItemStack extends ShopItem {

    private final ItemStack itemStack;

    public ShopItemStack(ItemStack itemStack, Double price) {
        super(price);
        this.itemStack = itemStack;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }
}
