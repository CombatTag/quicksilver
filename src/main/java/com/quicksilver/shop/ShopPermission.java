package com.quicksilver.shop;

import org.bukkit.permissions.Permission;

/**
 * Created by Derek on 12/25/2014.
 * Time: 10:03 PM
 */
public class ShopPermission extends ShopItem {

    private final Permission permission;

    public ShopPermission(Permission permission, Double price) {
        super(price);
        this.permission = permission;
    }

    public Permission getPermission() {
        return permission;
    }
}
