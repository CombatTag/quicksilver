package com.quicksilver.shop;

/**
 * Created by Derek on 12/25/2014.
 * Time: 10:19 AM
 */
public abstract class ShopItem {

    private final Double price;

    public ShopItem(Double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }
}
