package com.quicksilver.shop;

import com.quicksilver.QuickSilver;
import com.quicksilver.user.User;
import com.quicksilver.utils.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

import static com.quicksilver.utils.ChatUtil.t;

/**
 * Created by Derek on 12/25/2014.
 * Time: 10:12 AM
 */
public class ShopManager implements Listener {

    private HashMap<String, ShopItemStack> shopItems = new HashMap<>();
    private ArrayList<Inventory> shopPages = new ArrayList<>();

    public ShopManager(ConfigurationSection configurationSection) {
        loadShopItems(configurationSection);
        loadShopItems();
        loadShopGui();
    }

    public void buyItem(User user, ShopItem shopItem) {
        if (shopItem.getPrice() > user.getBalance()) {
            user.sendMessage(t("notEnough", String.valueOf(user.getBalance()), String.valueOf(shopItem.getPrice())));
            return;
        }
        if (!user.isOnline()) {
            user.sendMessage(t("notOnline", user.getLastUsername()));
            return;
        }

        if (shopItem instanceof ShopItemStack) {
            buyItemStack(user, (ShopItemStack) shopItem);
            return;
        }
        if (shopItem instanceof ShopPermission) {
            buyPermission(user, (ShopPermission) shopItem);
            return;
        }
        user.sendMessage(t("shopItemNotFound"));
    }

    private void buyItemStack(User user, ShopItemStack shopItemStack) {
        Player player = user.getPlayer();

        int availableAmount = ItemUtil.getAvailableAmount(player.getInventory(), shopItemStack.getItemStack());
        if (shopItemStack.getItemStack().getAmount() > availableAmount) {
            user.sendMessage(t("notEnoughSpace", String.valueOf(availableAmount), String.valueOf(shopItemStack.getItemStack().getAmount())));
            return;
        }
        user.setBalance(user.getBalance() - shopItemStack.getPrice());
        player.getInventory().addItem(shopItemStack.getItemStack());
        user.sendMessage(t("bought", shopItemStack.getItemStack().getItemMeta().getDisplayName(), String.valueOf(shopItemStack.getPrice()), String.valueOf(user.getBalance())));
    }

    private void buyPermission(User user, ShopPermission shopPermission) {
        Player player = user.getPlayer();

        if (player.hasPermission(shopPermission.getPermission())) {
            user.sendMessage(t("alreadyHavePermission", shopPermission.getPermission().getName()));
            return;
        }
        user.setBalance(user.getBalance() - shopPermission.getPrice());
        QuickSilver.p.getPermission().playerAdd(player, shopPermission.getPermission().getName());
        user.sendMessage(t("bought", shopPermission.getPermission().getName(), String.valueOf(shopPermission.getPrice()), String.valueOf(user.getBalance())));
    }

    public ArrayList<Inventory> getShopPages() {
        return shopPages;
    }

    private void loadShopGui() {
        int i = 0;
        for (ShopItem shopItem : shopItems.values()) {
            if (shopItem instanceof ShopItemStack) {
                ShopItemStack shopItemStack = (ShopItemStack) shopItem;
                if (i % 45 == 0) {
                    if (!shopPages.isEmpty()) {
                        Inventory inventory = shopPages.get(shopPages.size() - 1);

                        if (inventory != null) {
                            inventory.setItem(45, new ItemStack(Material.CHEST));
                            inventory.setItem(53, new ItemStack(Material.CHEST));
                        }
                    }

                    shopPages.add(Bukkit.createInventory(null, 54, "Shop Page " + (shopPages.size() + 1)));
                }
                Inventory inventory = shopPages.get(shopPages.size() - 1);

                ItemStack itemStack = shopItemStack.getItemStack().clone();
                ItemMeta itemMeta = itemStack.getItemMeta();

                itemMeta.setLore(Arrays.asList("\2477" + Math.round(shopItemStack.getPrice() * 1000.000) / 1000.000 + "\247a Per Item",
                        "\2477" + Math.round((shopItemStack.getPrice() * 64) * 1000.000) / 1000.000 + "\247a Per Stack"));

                itemStack.setItemMeta(itemMeta);

                inventory.addItem(itemStack);
                i++;
            }
        }
    }

    private void loadShopItems() {
        shopItems.put(ShopPaths.COAL, new ShopItemStack(new ItemStack(Material.COAL, 1), .85));

        double fuel = shopItems.get(ShopPaths.COAL).getPrice() / 8;

        shopItems.put(ShopPaths.NETHER_QUARTZ_ORE, new ShopItemStack(new ItemStack(Material.QUARTZ_ORE, 1), .09));
        shopItems.put(ShopPaths.NETHER_QUARTZ, new ShopItemStack(new ItemStack(Material.QUARTZ), shopItems.get(ShopPaths.NETHER_QUARTZ_ORE).getPrice()));
        shopItems.put(ShopPaths.COBBLESTONE, new ShopItemStack(new ItemStack(Material.COBBLESTONE), .0075));
        shopItems.put(ShopPaths.STONE, new ShopItemStack(new ItemStack(Material.STONE), shopItems.get(ShopPaths.COBBLESTONE).getPrice() + fuel));
        shopItems.put(ShopPaths.DIORITE, new ShopItemStack(new ItemStack(Material.STONE, 1, (byte) 3), shopItems.get(ShopPaths.COBBLESTONE).getPrice() + shopItems.get(ShopPaths.NETHER_QUARTZ).getPrice()));
        shopItems.put(ShopPaths.GRANITE, new ShopItemStack(new ItemStack(Material.STONE, 1, (byte) 1), shopItems.get(ShopPaths.DIORITE).getPrice() + shopItems.get(ShopPaths.NETHER_QUARTZ).getPrice()));
        shopItems.put(ShopPaths.POLISHED_GRANITE, new ShopItemStack(new ItemStack(Material.STONE, 1, (byte) 2), shopItems.get(ShopPaths.GRANITE).getPrice()));
        shopItems.put(ShopPaths.POLISHED_DIORITE, new ShopItemStack(new ItemStack(Material.STONE, 1, (byte) 4), shopItems.get(ShopPaths.DIORITE).getPrice()));
        shopItems.put(ShopPaths.ANDESITE, new ShopItemStack(new ItemStack(Material.STONE, 1, (byte) 5), shopItems.get(ShopPaths.DIORITE).getPrice() + shopItems.get(ShopPaths.COBBLESTONE).getPrice() / 2));
        shopItems.put(ShopPaths.POLISHED_ANDESITE, new ShopItemStack(new ItemStack(Material.STONE, 1, (byte) 6), shopItems.get(ShopPaths.ANDESITE).getPrice()));
        shopItems.put(ShopPaths.GRASS, new ShopItemStack(new ItemStack(Material.GRASS, 1), .015));
        shopItems.put(ShopPaths.DIRT, new ShopItemStack(new ItemStack(Material.DIRT, 1), .0075));
        shopItems.put(ShopPaths.COARSE_DIRT, new ShopItemStack(new ItemStack(Material.DIRT, 1, (byte) 1), .0075));
        shopItems.put(ShopPaths.PODZOL, new ShopItemStack(new ItemStack(Material.DIRT, 1, (byte) 2), .015));
        shopItems.put(ShopPaths.OAK_WOOD, new ShopItemStack(new ItemStack(Material.LOG, 1), .06));
        shopItems.put(ShopPaths.SPRUCE_WOOD, new ShopItemStack(new ItemStack(Material.LOG, 1, (byte) 1), shopItems.get(ShopPaths.OAK_WOOD).getPrice()));
        shopItems.put(ShopPaths.BIRCH_WOOD, new ShopItemStack(new ItemStack(Material.LOG, 1, (byte) 2), shopItems.get(ShopPaths.OAK_WOOD).getPrice()));
        shopItems.put(ShopPaths.JUNGLE_WOOD, new ShopItemStack(new ItemStack(Material.LOG, 1, (byte) 3), shopItems.get(ShopPaths.OAK_WOOD).getPrice()));
        shopItems.put(ShopPaths.ACACIA_WOOD, new ShopItemStack(new ItemStack(Material.LOG_2, 1), shopItems.get(ShopPaths.OAK_WOOD).getPrice()));
        shopItems.put(ShopPaths.DARK_OAK_WOOD, new ShopItemStack(new ItemStack(Material.LOG_2, 1, (byte) 1), shopItems.get(ShopPaths.OAK_WOOD).getPrice()));
        shopItems.put(ShopPaths.OAK_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD, 1), shopItems.get(ShopPaths.OAK_WOOD).getPrice() / 4));
        shopItems.put(ShopPaths.SPRUCE_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD, 1, (byte) 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice()));
        shopItems.put(ShopPaths.BIRCH_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD, 1, (byte) 2), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice()));
        shopItems.put(ShopPaths.JUNGLE_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD, 1, (byte) 3), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice()));
        shopItems.put(ShopPaths.ACACIA_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD, 1, (byte) 4), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice()));
        shopItems.put(ShopPaths.DARK_OAK_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD, 1, (byte) 5), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice()));
        shopItems.put(ShopPaths.OAK_SAPLING, new ShopItemStack(new ItemStack(Material.SAPLING, 1), shopItems.get(ShopPaths.OAK_WOOD).getPrice() / 2));
        shopItems.put(ShopPaths.SPRUCE_SAPLING, new ShopItemStack(new ItemStack(Material.SAPLING, 1, (byte) 1), shopItems.get(ShopPaths.OAK_SAPLING).getPrice()));
        shopItems.put(ShopPaths.BIRCH_SAPLING, new ShopItemStack(new ItemStack(Material.SAPLING, 1, (byte) 2), shopItems.get(ShopPaths.OAK_SAPLING).getPrice()));
        shopItems.put(ShopPaths.JUNGLE_SAPLING, new ShopItemStack(new ItemStack(Material.SAPLING, 1, (byte) 3), shopItems.get(ShopPaths.OAK_SAPLING).getPrice()));
        shopItems.put(ShopPaths.ACACIA_SAPLING, new ShopItemStack(new ItemStack(Material.SAPLING, 1, (byte) 4), shopItems.get(ShopPaths.OAK_SAPLING).getPrice()));
        shopItems.put(ShopPaths.DARK_OAK_SAPLING, new ShopItemStack(new ItemStack(Material.SAPLING, 1, (byte) 5), shopItems.get(ShopPaths.OAK_SAPLING).getPrice()));
        shopItems.put(ShopPaths.SAND, new ShopItemStack(new ItemStack(Material.SAND, 1), .0075));
        shopItems.put(ShopPaths.RED_SAND, new ShopItemStack(new ItemStack(Material.SAND, 1, (byte) 1), shopItems.get(ShopPaths.SAND).getPrice()));
        shopItems.put(ShopPaths.GRAVEL, new ShopItemStack(new ItemStack(Material.GRAVEL, 1), .015));
        shopItems.put(ShopPaths.GOLD_ORE, new ShopItemStack(new ItemStack(Material.GOLD_ORE, 1), 5.85));
        shopItems.put(ShopPaths.IRON_ORE, new ShopItemStack(new ItemStack(Material.IRON_ORE, 1), 1.6));
        shopItems.put(ShopPaths.COAL_ORE, new ShopItemStack(new ItemStack(Material.COAL_ORE, 1), .85));
        shopItems.put(ShopPaths.OAK_LEAVES, new ShopItemStack(new ItemStack(Material.LEAVES, 1), .0075));
        shopItems.put(ShopPaths.SPRUCE_LEAVES, new ShopItemStack(new ItemStack(Material.LEAVES, 1, (byte) 1), shopItems.get(ShopPaths.OAK_LEAVES).getPrice()));
        shopItems.put(ShopPaths.BIRCH_LEAVES, new ShopItemStack(new ItemStack(Material.LEAVES, 1, (byte) 2), shopItems.get(ShopPaths.OAK_LEAVES).getPrice()));
        shopItems.put(ShopPaths.JUNGLE_LEAVES, new ShopItemStack(new ItemStack(Material.LEAVES, 1, (byte) 3), shopItems.get(ShopPaths.OAK_LEAVES).getPrice()));
        shopItems.put(ShopPaths.ACACIA_LEAVES, new ShopItemStack(new ItemStack(Material.LEAVES_2, 1), shopItems.get(ShopPaths.OAK_LEAVES).getPrice()));
        shopItems.put(ShopPaths.DARK_OAK_LEAVES, new ShopItemStack(new ItemStack(Material.LEAVES_2, 1, (byte) 1), shopItems.get(ShopPaths.OAK_LEAVES).getPrice()));
        shopItems.put(ShopPaths.SPONGE, new ShopItemStack(new ItemStack(Material.SPONGE, 1), 5.0));
        shopItems.put(ShopPaths.WET_SPONGE, new ShopItemStack(new ItemStack(Material.SPONGE, 1, (byte) 1), shopItems.get(ShopPaths.SPONGE).getPrice()));
        shopItems.put(ShopPaths.GLASS, new ShopItemStack(new ItemStack(Material.GLASS, 1), shopItems.get(ShopPaths.SAND).getPrice() + fuel));
        shopItems.put(ShopPaths.LAPIS_LAZULI_ORE, new ShopItemStack(new ItemStack(Material.LAPIS_ORE, 1), 1.5));
        shopItems.put(ShopPaths.DANDELION, new ShopItemStack(new ItemStack(Material.YELLOW_FLOWER, 1), .015));
        shopItems.put(ShopPaths.POPPY, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.BLUE_ORCHID, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 1), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.ALLIUM, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 2), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.AZURE_BLUET, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 3), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.RED_TULIP, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 4), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.ORANGE_TULIP, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 5), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.WHITE_TULIP, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 6), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.PINK_TULIP, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 7), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.OXEYE_DAISY, new ShopItemStack(new ItemStack(Material.RED_ROSE, 1, (byte) 8), shopItems.get(ShopPaths.DANDELION).getPrice()));
        shopItems.put(ShopPaths.INK_SACK, new ShopItemStack(new ItemStack(Material.INK_SACK, 1), .015));
        shopItems.put(ShopPaths.BONE_MEAL, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 15), .015));
        shopItems.put(ShopPaths.BONE, new ShopItemStack(new ItemStack(Material.BONE, 1), shopItems.get(ShopPaths.BONE_MEAL).getPrice() * 3));
        shopItems.put(ShopPaths.ROSE_RED, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 1), shopItems.get(ShopPaths.POPPY).getPrice()));
        shopItems.put(ShopPaths.CACTUS, new ShopItemStack(new ItemStack(Material.CACTUS, 1), .02));
        shopItems.put(ShopPaths.CACTUS_GREEN, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 2), shopItems.get(ShopPaths.CACTUS).getPrice() + fuel));
        shopItems.put(ShopPaths.COCO_BEANS, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 3), .015));
        shopItems.put(ShopPaths.LAPIS_LAZULI, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 4), shopItems.get(ShopPaths.LAPIS_LAZULI_ORE).getPrice() / 6));
        shopItems.put(ShopPaths.PURPLE_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 5), (shopItems.get(ShopPaths.LAPIS_LAZULI).getPrice() + shopItems.get(ShopPaths.ROSE_RED).getPrice()) / 2));
        shopItems.put(ShopPaths.CYAN_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 6), (shopItems.get(ShopPaths.LAPIS_LAZULI).getPrice() + shopItems.get(ShopPaths.CACTUS_GREEN).getPrice()) / 2));
        shopItems.put(ShopPaths.LIGHT_GRAY_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 7), shopItems.get(ShopPaths.OXEYE_DAISY).getPrice()));
        shopItems.put(ShopPaths.GRAY_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 8), (shopItems.get(ShopPaths.INK_SACK).getPrice() + shopItems.get(ShopPaths.BONE_MEAL).getPrice()) / 2));
        shopItems.put(ShopPaths.PINK_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 9), (shopItems.get(ShopPaths.BONE_MEAL).getPrice() + shopItems.get(ShopPaths.ROSE_RED).getPrice()) / 2));
        shopItems.put(ShopPaths.LIME_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 10), (shopItems.get(ShopPaths.CACTUS_GREEN).getPrice() + shopItems.get(ShopPaths.BONE_MEAL).getPrice()) / 2));
        shopItems.put(ShopPaths.DANDELION_YELLOW, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 11), (shopItems.get(ShopPaths.DANDELION).getPrice())));
        shopItems.put(ShopPaths.LIGHT_BLUE_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 12), shopItems.get(ShopPaths.BLUE_ORCHID).getPrice()));
        shopItems.put(ShopPaths.MAGENTA_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 13), shopItems.get(ShopPaths.ALLIUM).getPrice()));
        shopItems.put(ShopPaths.ORANGE_DYE, new ShopItemStack(new ItemStack(Material.INK_SACK, 1, (byte) 14), shopItems.get(ShopPaths.ORANGE_TULIP).getPrice()));
        shopItems.put(ShopPaths.LAPIS_LAZULI_BLOCK, new ShopItemStack(new ItemStack(Material.LAPIS_BLOCK, 1), shopItems.get(ShopPaths.LAPIS_LAZULI).getPrice() * 9));
        shopItems.put(ShopPaths.STICK, new ShopItemStack(new ItemStack(Material.STICK, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() / 2));
        shopItems.put(ShopPaths.STRING, new ShopItemStack(new ItemStack(Material.STRING, 1), .0075));
        shopItems.put(ShopPaths.BOW, new ShopItemStack(new ItemStack(Material.BOW, 1), shopItems.get(ShopPaths.STRING).getPrice() * 3 + shopItems.get(ShopPaths.STRING).getPrice() * 3));
        shopItems.put(ShopPaths.REDSTONE_ORE, new ShopItemStack(new ItemStack(Material.REDSTONE_ORE, 1), 1.2));
        shopItems.put(ShopPaths.REDSTONE, new ShopItemStack(new ItemStack(Material.REDSTONE, 1), shopItems.get(ShopPaths.REDSTONE_ORE).getPrice() / 4.5));
        shopItems.put(ShopPaths.DISPENSER, new ShopItemStack(new ItemStack(Material.DISPENSER, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 7 + shopItems.get(ShopPaths.BOW).getPrice() + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.SANDSTONE, new ShopItemStack(new ItemStack(Material.SANDSTONE, 1), shopItems.get(ShopPaths.SAND).getPrice() * 4));
        shopItems.put(ShopPaths.CHISELED_SANDSTONE, new ShopItemStack(new ItemStack(Material.SANDSTONE, 1, (byte) 1), shopItems.get(ShopPaths.SANDSTONE).getPrice()));
        shopItems.put(ShopPaths.SMOOTH_SANDSTONE, new ShopItemStack(new ItemStack(Material.SANDSTONE, 1, (byte) 2), shopItems.get(ShopPaths.SANDSTONE).getPrice()));
        shopItems.put(ShopPaths.NOTE_BLOCK, new ShopItemStack(new ItemStack(Material.NOTE_BLOCK, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 8 + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.GOLD_INGOT, new ShopItemStack(new ItemStack(Material.GOLD_INGOT, 1), shopItems.get(ShopPaths.GOLD_ORE).getPrice() + fuel));
        shopItems.put(ShopPaths.POWERED_RAIL, new ShopItemStack(new ItemStack(Material.POWERED_RAIL, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 6 + shopItems.get(ShopPaths.STICK).getPrice() + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.STONE_PRESSURE_PLATE, new ShopItemStack(new ItemStack(Material.STONE_PLATE, 1), shopItems.get(ShopPaths.STONE).getPrice() * 2));
        shopItems.put(ShopPaths.IRON_INGOT, new ShopItemStack(new ItemStack(Material.IRON_INGOT, 1), shopItems.get(ShopPaths.IRON_ORE).getPrice() + fuel));
        shopItems.put(ShopPaths.DETECTOR_RAIL, new ShopItemStack(new ItemStack(Material.DETECTOR_RAIL, 1), (shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 6 + shopItems.get(ShopPaths.STONE_PRESSURE_PLATE).getPrice() + shopItems.get(ShopPaths.REDSTONE).getPrice()) / 6));
        shopItems.put(ShopPaths.PISTON, new ShopItemStack(new ItemStack(Material.PISTON_BASE, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 4 + shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 3 + shopItems.get(ShopPaths.IRON_INGOT).getPrice() + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.SLIMEBALL, new ShopItemStack(new ItemStack(Material.SLIME_BALL, 1), .0075));
        shopItems.put(ShopPaths.SLIME_BLOCK, new ShopItemStack(new ItemStack(Material.SLIME_BLOCK, 1), shopItems.get(ShopPaths.SLIMEBALL).getPrice() * 9));
        shopItems.put(ShopPaths.STICKY_PISTON, new ShopItemStack(new ItemStack(Material.PISTON_STICKY_BASE, 1), shopItems.get(ShopPaths.PISTON).getPrice() + shopItems.get(ShopPaths.SLIME_BLOCK).getPrice()));
        shopItems.put(ShopPaths.WHITE_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1), shopItems.get(ShopPaths.STRING).getPrice() * 4));
        shopItems.put(ShopPaths.ORANGE_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 1), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.ORANGE_DYE).getPrice()));
        shopItems.put(ShopPaths.MAGENTA_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 2), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.MAGENTA_DYE).getPrice()));
        shopItems.put(ShopPaths.LIGHT_BLUE_DYE, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 3), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.LIGHT_BLUE_DYE).getPrice()));
        shopItems.put(ShopPaths.YELLOW_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 4), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.DANDELION_YELLOW).getPrice()));
        shopItems.put(ShopPaths.LIME_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 5), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.LIME_DYE).getPrice()));
        shopItems.put(ShopPaths.PINK_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 6), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.PINK_DYE).getPrice()));
        shopItems.put(ShopPaths.GRAY_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 7), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.GRAY_DYE).getPrice()));
        shopItems.put(ShopPaths.LIGHT_GRAY_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 8), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.LIGHT_GRAY_DYE).getPrice()));
        shopItems.put(ShopPaths.CYAN_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 9), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.CYAN_DYE).getPrice()));
        shopItems.put(ShopPaths.PURPLE_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 10), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.PURPLE_DYE).getPrice()));
        shopItems.put(ShopPaths.BLUE_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 11), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.LAPIS_LAZULI).getPrice()));
        shopItems.put(ShopPaths.BROWN_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 12), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.COCO_BEANS).getPrice()));
        shopItems.put(ShopPaths.GREEN_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 13), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.CACTUS_GREEN).getPrice()));
        shopItems.put(ShopPaths.RED_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 14), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.ROSE_RED).getPrice()));
        shopItems.put(ShopPaths.BLACK_WOOL, new ShopItemStack(new ItemStack(Material.WOOL, 1, (byte) 15), shopItems.get(ShopPaths.WHITE_WOOL).getPrice() + shopItems.get(ShopPaths.INK_SACK).getPrice()));
        shopItems.put(ShopPaths.BROWN_MUSHROOM, new ShopItemStack(new ItemStack(Material.BROWN_MUSHROOM, 1), .015));
        shopItems.put(ShopPaths.RED_MUSHROOM, new ShopItemStack(new ItemStack(Material.RED_MUSHROOM, 1), .015));
        shopItems.put(ShopPaths.GOLD_BLOCK, new ShopItemStack(new ItemStack(Material.GOLD_BLOCK, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 9));
        shopItems.put(ShopPaths.IRON_BLOCK, new ShopItemStack(new ItemStack(Material.IRON_BLOCK, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 9));
        shopItems.put(ShopPaths.STONE_SLAB, new ShopItemStack(new ItemStack(Material.STEP, 1), shopItems.get(ShopPaths.STONE).getPrice() / 2));
        shopItems.put(ShopPaths.SANDSTONE_SLAB, new ShopItemStack(new ItemStack(Material.STEP, 1, (byte) 1), shopItems.get(ShopPaths.SANDSTONE).getPrice() / 2));
        shopItems.put(ShopPaths.COBBLESTONE_SLAB, new ShopItemStack(new ItemStack(Material.STEP, 1, (byte) 3), shopItems.get(ShopPaths.COBBLESTONE).getPrice() / 2));
        shopItems.put(ShopPaths.CLAY_BALL, new ShopItemStack(new ItemStack(Material.CLAY_BALL, 1), .0075));
        shopItems.put(ShopPaths.CLAY, new ShopItemStack(new ItemStack(Material.CLAY, 1), shopItems.get(ShopPaths.CLAY_BALL).getPrice() * 4));
        shopItems.put(ShopPaths.BRICK, new ShopItemStack(new ItemStack(Material.CLAY_BRICK, 1), shopItems.get(ShopPaths.CLAY_BALL).getPrice() + fuel));
        shopItems.put(ShopPaths.BRICKS, new ShopItemStack(new ItemStack(Material.BRICK, 1), shopItems.get(ShopPaths.BRICK).getPrice() * 4));
        shopItems.put(ShopPaths.BRICK_SLAB, new ShopItemStack(new ItemStack(Material.STEP, 1, (byte) 4), shopItems.get(ShopPaths.BRICKS).getPrice() / 2));
        shopItems.put(ShopPaths.STONE_BRICKS, new ShopItemStack(new ItemStack(Material.SMOOTH_BRICK, 1), shopItems.get(ShopPaths.STONE).getPrice()));
        shopItems.put(ShopPaths.STONE_BRICK_SLAB, new ShopItemStack(new ItemStack(Material.STEP, 1, (byte) 5), shopItems.get(ShopPaths.STONE_BRICKS).getPrice() / 2));
        shopItems.put(ShopPaths.NETHERRACK, new ShopItemStack(new ItemStack(Material.NETHERRACK, 1), .0075));
        shopItems.put(ShopPaths.NETHER_BRICK_ITEM, new ShopItemStack(new ItemStack(Material.NETHER_BRICK_ITEM, 1), shopItems.get(ShopPaths.NETHERRACK).getPrice() + fuel));
        shopItems.put(ShopPaths.NETHER_BRICK, new ShopItemStack(new ItemStack(Material.NETHER_BRICK, 1), shopItems.get(ShopPaths.NETHER_BRICK_ITEM).getPrice()));
        shopItems.put(ShopPaths.NETHER_BRICK_SLAB, new ShopItemStack(new ItemStack(Material.STEP, 1, (byte) 6), shopItems.get(ShopPaths.NETHER_BRICK).getPrice() / 2));
        shopItems.put(ShopPaths.QUARTZ_BLOCK, new ShopItemStack(new ItemStack(Material.QUARTZ_BLOCK, 1), shopItems.get(ShopPaths.NETHER_QUARTZ).getPrice() * 4));
        shopItems.put(ShopPaths.QUARTZ_SLAB, new ShopItemStack(new ItemStack(Material.STEP, 1, (byte) 7), shopItems.get(ShopPaths.QUARTZ_BLOCK).getPrice() / 2));
        shopItems.put(ShopPaths.GUNPOWDER, new ShopItemStack(new ItemStack(Material.SULPHUR), 0.05));
        shopItems.put(ShopPaths.TNT, new ShopItemStack(new ItemStack(Material.TNT, 1), shopItems.get(ShopPaths.GUNPOWDER).getPrice() * 5 + shopItems.get(ShopPaths.SAND).getPrice() * 4));
        shopItems.put(ShopPaths.SUGAR_CANES, new ShopItemStack(new ItemStack(Material.SUGAR_CANE, 1), .015));
        shopItems.put(ShopPaths.SUGAR, new ShopItemStack(new ItemStack(Material.SUGAR, 1), shopItems.get(ShopPaths.SUGAR_CANES).getPrice()));
        shopItems.put(ShopPaths.PAPER, new ShopItemStack(new ItemStack(Material.PAPER, 1), shopItems.get(ShopPaths.SUGAR_CANES).getPrice()));
        shopItems.put(ShopPaths.RABBIT_HIDE, new ShopItemStack(new ItemStack(Material.RABBIT_HIDE, 1), .00375));
        shopItems.put(ShopPaths.LEATHER, new ShopItemStack(new ItemStack(Material.LEATHER, 1), shopItems.get(ShopPaths.RABBIT_HIDE).getPrice() * 4));
        shopItems.put(ShopPaths.BOOK, new ShopItemStack(new ItemStack(Material.BOOK, 1), shopItems.get(ShopPaths.PAPER).getPrice() * 3 + shopItems.get(ShopPaths.LEATHER).getPrice()));
        shopItems.put(ShopPaths.BOOKSHELF, new ShopItemStack(new ItemStack(Material.BOOKSHELF, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 6 + shopItems.get(ShopPaths.BOOK).getPrice() * 3));
        shopItems.put(ShopPaths.VINES, new ShopItemStack(new ItemStack(Material.VINE, 1), .0075));
        shopItems.put(ShopPaths.MOSS_STONE, new ShopItemStack(new ItemStack(Material.MOSSY_COBBLESTONE, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() + shopItems.get(ShopPaths.VINES).getPrice()));
        shopItems.put(ShopPaths.OBSIDIAN, new ShopItemStack(new ItemStack(Material.OBSIDIAN, 1), .05));
        shopItems.put(ShopPaths.TORCH, new ShopItemStack(new ItemStack(Material.TORCH, 1), (shopItems.get(ShopPaths.COAL).getPrice() + shopItems.get(ShopPaths.STICK).getPrice()) / 4));
        shopItems.put(ShopPaths.OAK_WOOD_STAIRS, new ShopItemStack(new ItemStack(Material.WOOD_STAIRS, 1), (shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.CHEST, new ShopItemStack(new ItemStack(Material.CHEST, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 8));
        shopItems.put(ShopPaths.DIAMOND_ORE, new ShopItemStack(new ItemStack(Material.DIAMOND_ORE, 1), 9.9));
        shopItems.put(ShopPaths.DIAMOND, new ShopItemStack(new ItemStack(Material.DIAMOND, 1), shopItems.get(ShopPaths.DIAMOND_ORE).getPrice()));
        shopItems.put(ShopPaths.DIAMOND_BLOCK, new ShopItemStack(new ItemStack(Material.DIAMOND_BLOCK, 1), shopItems.get(ShopPaths.DIAMOND).getPrice() * 9));
        shopItems.put(ShopPaths.CRAFTING_TABLE, new ShopItemStack(new ItemStack(Material.WORKBENCH, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 4));
        shopItems.put(ShopPaths.FURNACE, new ShopItemStack(new ItemStack(Material.FURNACE, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 8));
        shopItems.put(ShopPaths.OAK_DOOR, new ShopItemStack(new ItemStack(Material.WOOD_DOOR, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 6));
        shopItems.put(ShopPaths.LADDER, new ShopItemStack(new ItemStack(Material.LADDER, 1), (shopItems.get(ShopPaths.STICK).getPrice() * 7) / 3));
        shopItems.put(ShopPaths.RAIL, new ShopItemStack(new ItemStack(Material.RAILS, 1), (shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 6 + shopItems.get(ShopPaths.STICK).getPrice()) / 16));
        shopItems.put(ShopPaths.COBBLESTONE_STAIRS, new ShopItemStack(new ItemStack(Material.COBBLESTONE_STAIRS, 1), (shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.LEVER, new ShopItemStack(new ItemStack(Material.LEVER, 1), shopItems.get(ShopPaths.STICK).getPrice() + shopItems.get(ShopPaths.COBBLESTONE).getPrice()));
        shopItems.put(ShopPaths.STONE_PRESSURE_PLATE, new ShopItemStack(new ItemStack(Material.STONE_PLATE, 1), shopItems.get(ShopPaths.STONE).getPrice() * 2));
        shopItems.put(ShopPaths.WOODEN_PRESSURE_PLATE, new ShopItemStack(new ItemStack(Material.WOOD_PLATE, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 2));
        shopItems.put(ShopPaths.IRON_DOOR, new ShopItemStack(new ItemStack(Material.IRON_DOOR, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 6));
        shopItems.put(ShopPaths.REDSTONE_TORCH_ON, new ShopItemStack(new ItemStack(Material.TORCH, 1), shopItems.get(ShopPaths.STICK).getPrice() + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.WOODEN_BUTTON, new ShopItemStack(new ItemStack(Material.WOOD_BUTTON, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice()));
        shopItems.put(ShopPaths.STONE_BUTTON, new ShopItemStack(new ItemStack(Material.STONE_BUTTON, 1), shopItems.get(ShopPaths.STONE).getPrice()));
        shopItems.put(ShopPaths.SNOWBALL, new ShopItemStack(new ItemStack(Material.SNOW_BALL, 1), .00375));
        shopItems.put(ShopPaths.SNOW_BLOCK, new ShopItemStack(new ItemStack(Material.SNOW_BLOCK, 1), shopItems.get(ShopPaths.SNOWBALL).getPrice() * 4));
        shopItems.put(ShopPaths.SNOW, new ShopItemStack(new ItemStack(Material.SNOW, 1), shopItems.get(ShopPaths.SNOW_BLOCK).getPrice() / 2));
        shopItems.put(ShopPaths.ICE, new ShopItemStack(new ItemStack(Material.ICE, 1), .015));
        shopItems.put(ShopPaths.PACKED_ICE, new ShopItemStack(new ItemStack(Material.PACKED_ICE, 1), shopItems.get(ShopPaths.ICE).getPrice() * 4));
        shopItems.put(ShopPaths.JUKEBOX, new ShopItemStack(new ItemStack(Material.JUKEBOX, 1), (shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 8) + shopItems.get(ShopPaths.DIAMOND).getPrice()));
        shopItems.put(ShopPaths.OAK_FENCE, new ShopItemStack(new ItemStack(Material.FENCE, 1), ((shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 4) + (shopItems.get(ShopPaths.STICK).getPrice() * 2)) / 3));
        shopItems.put(ShopPaths.PUMPKIN, new ShopItemStack(new ItemStack(Material.PUMPKIN, 1), .015));
        shopItems.put(ShopPaths.SOUL_SAND, new ShopItemStack(new ItemStack(Material.SOUL_SAND, 1), .015));
        shopItems.put(ShopPaths.GLOWSTONE_DUST, new ShopItemStack(new ItemStack(Material.GLOWSTONE_DUST, 1), .0075));
        shopItems.put(ShopPaths.GLOWSTONE, new ShopItemStack(new ItemStack(Material.GLOWSTONE, 1), shopItems.get(ShopPaths.GLOWSTONE_DUST).getPrice() * 4));
        shopItems.put(ShopPaths.JACK_OLANTERN, new ShopItemStack(new ItemStack(Material.JACK_O_LANTERN, 1), shopItems.get(ShopPaths.PUMPKIN).getPrice() + shopItems.get(ShopPaths.TORCH).getPrice()));
        shopItems.put(ShopPaths.WHITE_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.BONE_MEAL).getPrice()));
        shopItems.put(ShopPaths.ORANGE_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 1), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.ORANGE_DYE).getPrice()));
        shopItems.put(ShopPaths.MAGENTA_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 2), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.MAGENTA_DYE).getPrice()));
        shopItems.put(ShopPaths.LIGHT_BLUE_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 3), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.LIGHT_BLUE_DYE).getPrice()));
        shopItems.put(ShopPaths.YELLOW_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 4), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.DANDELION_YELLOW).getPrice()));
        shopItems.put(ShopPaths.LIME_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 5), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.LIME_DYE).getPrice()));
        shopItems.put(ShopPaths.PINK_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 6), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.PINK_DYE).getPrice()));
        shopItems.put(ShopPaths.GRAY_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 7), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.GRAY_DYE).getPrice()));
        shopItems.put(ShopPaths.LIGHT_GRAY_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 8), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.LIGHT_GRAY_DYE).getPrice()));
        shopItems.put(ShopPaths.CYAN_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 9), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.CYAN_DYE).getPrice()));
        shopItems.put(ShopPaths.PURPLE_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 10), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.PURPLE_DYE).getPrice()));
        shopItems.put(ShopPaths.BLUE_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 11), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.LAPIS_LAZULI).getPrice()));
        shopItems.put(ShopPaths.BROWN_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 12), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.COCO_BEANS).getPrice()));
        shopItems.put(ShopPaths.GREEN_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 13), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.CACTUS_GREEN).getPrice()));
        shopItems.put(ShopPaths.RED_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 14), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.ROSE_RED).getPrice()));
        shopItems.put(ShopPaths.BLACK_STAINED_GLASS, new ShopItemStack(new ItemStack(Material.STAINED_GLASS, 1, (byte) 15), shopItems.get(ShopPaths.GLASS).getPrice() * 8 + shopItems.get(ShopPaths.INK_SACK).getPrice()));
        shopItems.put(ShopPaths.WOODEN_TRAPDOOR, new ShopItemStack(new ItemStack(Material.TRAP_DOOR, 1), (shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 6) / 2));
        shopItems.put(ShopPaths.MOSSY_STONE_BRICKS, new ShopItemStack(new ItemStack(Material.SMOOTH_BRICK, 1, (byte) 1), shopItems.get(ShopPaths.VINES).getPrice() + shopItems.get(ShopPaths.STONE_BRICKS).getPrice()));
        shopItems.put(ShopPaths.CRACKED_STONE_BRICKS, new ShopItemStack(new ItemStack(Material.SMOOTH_BRICK, 1, (byte) 2), shopItems.get(ShopPaths.STONE_BRICKS).getPrice()));
        shopItems.put(ShopPaths.CHISELED_STONE_BRICKS, new ShopItemStack(new ItemStack(Material.SMOOTH_BRICK, 1, (byte) 3), shopItems.get(ShopPaths.STONE_BRICK_SLAB).getPrice() * 2));
        shopItems.put(ShopPaths.IRON_BARS, new ShopItemStack(new ItemStack(Material.IRON_BARDING, 1), (shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 6) / 16));
        shopItems.put(ShopPaths.GLASS_PANE, new ShopItemStack(new ItemStack(Material.THIN_GLASS, 1), (shopItems.get(ShopPaths.GLASS).getPrice() * 6) / 16));
        shopItems.put(ShopPaths.MELON, new ShopItemStack(new ItemStack(Material.MELON, 1), .02));
        shopItems.put(ShopPaths.MELON_BLOCK, new ShopItemStack(new ItemStack(Material.MELON_BLOCK, 1), shopItems.get(ShopPaths.MELON).getPrice() * 9));
        shopItems.put(ShopPaths.OAK_FENCE_GATE, new ShopItemStack(new ItemStack(Material.FENCE_GATE, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice() * 4));
        shopItems.put(ShopPaths.BRICK_STAIRS, new ShopItemStack(new ItemStack(Material.BRICK_STAIRS, 1), (shopItems.get(ShopPaths.BRICKS).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.STONE_BRICK_STAIRS, new ShopItemStack(new ItemStack(Material.SMOOTH_STAIRS, 1), (shopItems.get(ShopPaths.STONE_BRICKS).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.MYCELIUM, new ShopItemStack(new ItemStack(Material.MYCEL, 1), shopItems.get(ShopPaths.GRASS).getPrice() * 2));
        shopItems.put(ShopPaths.LILY_PAD, new ShopItemStack(new ItemStack(Material.WATER_LILY, 1), .0075));
        shopItems.put(ShopPaths.NETHER_BRICK_FENCE, new ShopItemStack(new ItemStack(Material.NETHER_FENCE, 1), shopItems.get(ShopPaths.NETHER_BRICK).getPrice()));
        shopItems.put(ShopPaths.NETHER_BRICK_STAIRS, new ShopItemStack(new ItemStack(Material.NETHER_BRICK_STAIRS, 1), (shopItems.get(ShopPaths.NETHER_BRICK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.ENCHANTMENT_TABLE, new ShopItemStack(new ItemStack(Material.ENCHANTMENT_TABLE, 1), shopItems.get(ShopPaths.OBSIDIAN).getPrice() * 4 + shopItems.get(ShopPaths.DIAMOND).getPrice() * 2 + shopItems.get(ShopPaths.BOOK).getPrice()));
        shopItems.put(ShopPaths.CAULDRON, new ShopItemStack(new ItemStack(Material.CAULDRON, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 7));
        shopItems.put(ShopPaths.END_STONE, new ShopItemStack(new ItemStack(Material.ENDER_STONE), .015));
        shopItems.put(ShopPaths.REDSTONE_LAMP_INACTIVE, new ShopItemStack(new ItemStack(Material.REDSTONE_LAMP_OFF, 1), shopItems.get(ShopPaths.REDSTONE).getPrice() * 4 + shopItems.get(ShopPaths.GLOWSTONE).getPrice()));
        shopItems.put(ShopPaths.OAK_WOOD_SLAB, new ShopItemStack(new ItemStack(Material.WOOD_STEP, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() / 2));
        shopItems.put(ShopPaths.SPRUCE_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD_STEP, 1, (byte) 1), shopItems.get(ShopPaths.SPRUCE_WOOD_PLANK).getPrice() / 2));
        shopItems.put(ShopPaths.BIRCH_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD_STEP, 1, (byte) 2), shopItems.get(ShopPaths.BIRCH_WOOD_PLANK).getPrice() / 2));
        shopItems.put(ShopPaths.JUNGLE_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD_STEP, 1, (byte) 3), shopItems.get(ShopPaths.JUNGLE_WOOD_PLANK).getPrice() / 2));
        shopItems.put(ShopPaths.ACACIA_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD_STEP, 1, (byte) 4), shopItems.get(ShopPaths.ACACIA_WOOD_PLANK).getPrice() / 2));
        shopItems.put(ShopPaths.DARK_OAK_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.WOOD_STEP, 1, (byte) 5), shopItems.get(ShopPaths.DARK_OAK_WOOD_PLANK).getPrice() / 2));
        shopItems.put(ShopPaths.SANDSTONE_STAIRS, new ShopItemStack(new ItemStack(Material.SANDSTONE_STAIRS, 1), (shopItems.get(ShopPaths.SANDSTONE).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.EMERALD_ORE, new ShopItemStack(new ItemStack(Material.EMERALD_ORE, 1), 61.4));
        shopItems.put(ShopPaths.EMERALD, new ShopItemStack(new ItemStack(Material.EMERALD, 1), shopItems.get(ShopPaths.EMERALD_ORE).getPrice()));
        shopItems.put(ShopPaths.EMERALD_BLOCK, new ShopItemStack(new ItemStack(Material.EMERALD_BLOCK, 1), shopItems.get(ShopPaths.EMERALD).getPrice() * 9));
        shopItems.put(ShopPaths.ENDER_PEARL, new ShopItemStack(new ItemStack(Material.ENDER_PEARL, 1), .02));
        shopItems.put(ShopPaths.BLAZE_ROD, new ShopItemStack(new ItemStack(Material.BLAZE_ROD, 1), .02));
        shopItems.put(ShopPaths.BLAZE_POWDER, new ShopItemStack(new ItemStack(Material.BLAZE_POWDER, 1), shopItems.get(ShopPaths.BLAZE_ROD).getPrice() / 2));
        shopItems.put(ShopPaths.EYE_OF_ENDER, new ShopItemStack(new ItemStack(Material.EYE_OF_ENDER, 1), shopItems.get(ShopPaths.ENDER_PEARL).getPrice() + shopItems.get(ShopPaths.BLAZE_POWDER).getPrice()));
        shopItems.put(ShopPaths.ENDER_CHEST, new ShopItemStack(new ItemStack(Material.ENDER_CHEST, 1), shopItems.get(ShopPaths.OBSIDIAN).getPrice() * 8 + shopItems.get(ShopPaths.EYE_OF_ENDER).getPrice()));
        shopItems.put(ShopPaths.TRIPWIRE_HOOK, new ShopItemStack(new ItemStack(Material.TRIPWIRE_HOOK, 1), (shopItems.get(ShopPaths.IRON_INGOT).getPrice() + shopItems.get(ShopPaths.STICK).getPrice() + shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice()) / 2));
        shopItems.put(ShopPaths.SPRUCE_WOOD_STAIRS, new ShopItemStack(new ItemStack(Material.SPRUCE_WOOD_STAIRS, 1), (shopItems.get(ShopPaths.SPRUCE_WOOD_PLANK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.BIRCH_WOOD_PLANK, new ShopItemStack(new ItemStack(Material.BIRCH_WOOD_STAIRS, 1), (shopItems.get(ShopPaths.BIRCH_WOOD_PLANK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.JUNGLE_WOOD_STAIRS, new ShopItemStack(new ItemStack(Material.JUNGLE_WOOD_STAIRS, 1), (shopItems.get(ShopPaths.JUNGLE_WOOD_PLANK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.NETHER_STAR, new ShopItemStack(new ItemStack(Material.NETHER_STAR, 1), shopItems.get(ShopPaths.DIAMOND).getPrice()));
        shopItems.put(ShopPaths.BEACON, new ShopItemStack(new ItemStack(Material.BEACON, 1), shopItems.get(ShopPaths.OBSIDIAN).getPrice() * 3 + shopItems.get(ShopPaths.GLASS).getPrice() * 5 + shopItems.get(ShopPaths.NETHER_STAR).getPrice()));
        shopItems.put(ShopPaths.COBBLESTONE_WALL, new ShopItemStack(new ItemStack(Material.COBBLE_WALL, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice()));
        shopItems.put(ShopPaths.MOSS_STONE, new ShopItemStack(new ItemStack(Material.MOSSY_COBBLESTONE, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() + shopItems.get(ShopPaths.VINES).getPrice()));
        shopItems.put(ShopPaths.MOSSY_COBBLESTONE_WALL, new ShopItemStack(new ItemStack(Material.COBBLE_WALL, 1, (byte) 1), shopItems.get(ShopPaths.MOSS_STONE).getPrice()));
        shopItems.put(ShopPaths.FLOWER_POT, new ShopItemStack(new ItemStack(Material.FLOWER_POT, 1), shopItems.get(ShopPaths.BRICK).getPrice() * 3));
        shopItems.put(ShopPaths.ANVIL, new ShopItemStack(new ItemStack(Material.ANVIL, 1), shopItems.get(ShopPaths.IRON_BLOCK).getPrice() * 3 + shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 4));
        shopItems.put(ShopPaths.WEIGHTED_PRESSURE_PLATE_LIGHT, new ShopItemStack(new ItemStack(Material.GOLD_PLATE, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 2));
        shopItems.put(ShopPaths.WEIGHTED_PRESSURE_PLATE_HEAVY, new ShopItemStack(new ItemStack(Material.IRON_PLATE, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 2));
        shopItems.put(ShopPaths.DAYLIGHT_SENSOR, new ShopItemStack(new ItemStack(Material.DAYLIGHT_DETECTOR, 1), shopItems.get(ShopPaths.GLASS).getPrice() * 3 + shopItems.get(ShopPaths.NETHER_QUARTZ).getPrice() * 3 + shopItems.get(ShopPaths.OAK_WOOD_SLAB).getPrice() * 3));
        shopItems.put(ShopPaths.REDSTONE_BLOCK, new ShopItemStack(new ItemStack(Material.REDSTONE_BLOCK, 1), shopItems.get(ShopPaths.REDSTONE).getPrice() * 9));
        shopItems.put(ShopPaths.HOPPER, new ShopItemStack(new ItemStack(Material.HOPPER, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 5 + shopItems.get(ShopPaths.CHEST).getPrice()));
        shopItems.put(ShopPaths.QUARTZ_BLOCK, new ShopItemStack(new ItemStack(Material.QUARTZ_BLOCK, 1), shopItems.get(ShopPaths.NETHER_QUARTZ).getPrice() * 4));
        shopItems.put(ShopPaths.CHISELED_QUARTZ_BLOCK, new ShopItemStack(new ItemStack(Material.QUARTZ_BLOCK, 1, (byte) 1), shopItems.get(ShopPaths.QUARTZ_SLAB).getPrice() * 2));
        shopItems.put(ShopPaths.PILLAR_QUARTZ_BLOCK, new ShopItemStack(new ItemStack(Material.QUARTZ, 1, (byte) 2), shopItems.get(ShopPaths.QUARTZ_BLOCK).getPrice()));
        shopItems.put(ShopPaths.QUARTZ_STAIRS, new ShopItemStack(new ItemStack(Material.QUARTZ_STAIRS, 1), (shopItems.get(ShopPaths.QUARTZ_BLOCK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.ACTIVATOR_RAIL, new ShopItemStack(new ItemStack(Material.ACTIVATOR_RAIL, 1), (shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 6 + shopItems.get(ShopPaths.STICK).getPrice() * 2 + shopItems.get(ShopPaths.REDSTONE_TORCH_ON).getPrice()) / 6));
        shopItems.put(ShopPaths.DROPPER, new ShopItemStack(new ItemStack(Material.DROPPER, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 7 + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.HARDENED_CLAY, new ShopItemStack(new ItemStack(Material.HARD_CLAY, 1), shopItems.get(ShopPaths.CLAY).getPrice() + fuel));
        shopItems.put(ShopPaths.WHITE_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.BONE_MEAL).getPrice()));
        shopItems.put(ShopPaths.ORANGE_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 1), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.ORANGE_DYE).getPrice()));
        shopItems.put(ShopPaths.MAGENTA_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 2), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.MAGENTA_DYE).getPrice()));
        shopItems.put(ShopPaths.LIGHT_BLUE_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 3), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.LIGHT_BLUE_DYE).getPrice()));
        shopItems.put(ShopPaths.YELLOW_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 4), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.DANDELION_YELLOW).getPrice()));
        shopItems.put(ShopPaths.LIME_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 5), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.LIME_DYE).getPrice()));
        shopItems.put(ShopPaths.PINK_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 6), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.PINK_DYE).getPrice()));
        shopItems.put(ShopPaths.GRAY_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 7), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.GRAY_DYE).getPrice()));
        shopItems.put(ShopPaths.LIGHT_GRAY_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 8), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.LIGHT_GRAY_DYE).getPrice()));
        shopItems.put(ShopPaths.CYAN_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 9), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.CYAN_DYE).getPrice()));
        shopItems.put(ShopPaths.PURPLE_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 10), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.PURPLE_DYE).getPrice()));
        shopItems.put(ShopPaths.BLUE_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 11), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.LAPIS_LAZULI).getPrice()));
        shopItems.put(ShopPaths.BROWN_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 12), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.COCO_BEANS).getPrice()));
        shopItems.put(ShopPaths.GREEN_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 13), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.CACTUS_GREEN).getPrice()));
        shopItems.put(ShopPaths.RED_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 14), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.ROSE_RED).getPrice()));
        shopItems.put(ShopPaths.BLACK_STAINED_CLAY, new ShopItemStack(new ItemStack(Material.STAINED_CLAY, 1, (byte) 15), shopItems.get(ShopPaths.HARDENED_CLAY).getPrice() * 8 + shopItems.get(ShopPaths.INK_SACK).getPrice()));
        shopItems.put(ShopPaths.ACACIA_WOOD_STAIRS, new ShopItemStack(new ItemStack(Material.ACACIA_STAIRS, 1), (shopItems.get(ShopPaths.ACACIA_WOOD_PLANK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.DARK_OAK_WOOD_STAIRS, new ShopItemStack(new ItemStack(Material.DARK_OAK_STAIRS, 1), (shopItems.get(ShopPaths.DARK_OAK_WOOD_PLANK).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.IRON_TRAPDOOR, new ShopItemStack(new ItemStack(Material.IRON_TRAPDOOR, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 4));
        shopItems.put(ShopPaths.PRISMARINE_SHARD, new ShopItemStack(new ItemStack(Material.PRISMARINE_SHARD, 1), .015));
        shopItems.put(ShopPaths.PRISMARINE_CRYSTALS, new ShopItemStack(new ItemStack(Material.PRISMARINE_CRYSTALS, 1), .03));
        shopItems.put(ShopPaths.PRISMARINE, new ShopItemStack(new ItemStack(Material.PRISMARINE, 1), shopItems.get(ShopPaths.PRISMARINE_SHARD).getPrice() * 4));
        shopItems.put(ShopPaths.PRISMARINE_BRICKS, new ShopItemStack(new ItemStack(Material.PRISMARINE, 1, (byte) 1), shopItems.get(ShopPaths.PRISMARINE_SHARD).getPrice() * 9));
        shopItems.put(ShopPaths.DARK_PRISMARINE, new ShopItemStack(new ItemStack(Material.PRISMARINE, 1, (byte) 2), shopItems.get(ShopPaths.PRISMARINE_SHARD).getPrice() * 8 + shopItems.get(ShopPaths.INK_SACK).getPrice()));
        shopItems.put(ShopPaths.SEA_LANTERN, new ShopItemStack(new ItemStack(Material.SEA_LANTERN, 1), shopItems.get(ShopPaths.PRISMARINE_SHARD).getPrice() * 4 + shopItems.get(ShopPaths.PRISMARINE_CRYSTALS).getPrice() * 5));
        shopItems.put(ShopPaths.WHEAT, new ShopItemStack(new ItemStack(Material.WHEAT, 1), .015));
        shopItems.put(ShopPaths.HAY_BALE, new ShopItemStack(new ItemStack(Material.HAY_BLOCK, 1), shopItems.get(ShopPaths.WHEAT).getPrice() * 9));
        shopItems.put(ShopPaths.BLOCK_OF_COAL, new ShopItemStack(new ItemStack(Material.COAL_BLOCK, 1), shopItems.get(ShopPaths.COAL).getPrice() * 9));
        shopItems.put(ShopPaths.SUNFLOWER, new ShopItemStack(new ItemStack(Material.DOUBLE_PLANT, 1), shopItems.get(ShopPaths.DANDELION).getPrice() * 2));
        shopItems.put(ShopPaths.LILAC, new ShopItemStack(new ItemStack(Material.DOUBLE_PLANT, 1, (byte) 1), shopItems.get(ShopPaths.ALLIUM).getPrice() * 2));
        shopItems.put(ShopPaths.ROSE_BUSH, new ShopItemStack(new ItemStack(Material.DOUBLE_PLANT, 1, (byte) 4), shopItems.get(ShopPaths.POPPY).getPrice() * 2));
        shopItems.put(ShopPaths.PEONY, new ShopItemStack(new ItemStack(Material.DOUBLE_PLANT, 1, (byte) 5), shopItems.get(ShopPaths.PINK_TULIP).getPrice() * 2));
        shopItems.put(ShopPaths.RED_SANDSTONE, new ShopItemStack(new ItemStack(Material.RED_SANDSTONE, 1), shopItems.get(ShopPaths.RED_SAND).getPrice() * 4));
        shopItems.put(ShopPaths.SMOOTH_RED_SANDSTONE, new ShopItemStack(new ItemStack(Material.RED_SANDSTONE, 1, (byte) 1), shopItems.get(ShopPaths.RED_SANDSTONE).getPrice()));
        shopItems.put(ShopPaths.RED_SANDSTONE_SLAB, new ShopItemStack(new ItemStack(Material.STONE_SLAB2, 1), shopItems.get(ShopPaths.RED_SANDSTONE).getPrice() / 2));
        shopItems.put(ShopPaths.CHISELED_RED_SANDSTONE, new ShopItemStack(new ItemStack(Material.RED_SANDSTONE, 1, (byte) 2), shopItems.get(ShopPaths.RED_SANDSTONE_SLAB).getPrice() * 2));
        shopItems.put(ShopPaths.RED_SANDSTONE_STAIRS, new ShopItemStack(new ItemStack(Material.RED_SANDSTONE_STAIRS, 1), (shopItems.get(ShopPaths.RED_SANDSTONE).getPrice() * 6) / 4));
        shopItems.put(ShopPaths.IRON_SHOVEL, new ShopItemStack(new ItemStack(Material.IRON_SPADE, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.IRON_PICKAXE, new ShopItemStack(new ItemStack(Material.IRON_PICKAXE, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.IRON_AXE, new ShopItemStack(new ItemStack(Material.IRON_AXE, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.FLINT, new ShopItemStack(new ItemStack(Material.FLINT, 1), .01));
        shopItems.put(ShopPaths.FLINT_AND_STEEL, new ShopItemStack(new ItemStack(Material.FLINT_AND_STEEL, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() + shopItems.get(ShopPaths.FLINT).getPrice()));
        shopItems.put(ShopPaths.APPLE, new ShopItemStack(new ItemStack(Material.APPLE, 1), .02));
        shopItems.put(ShopPaths.BOW, new ShopItemStack(new ItemStack(Material.BOW, 1), shopItems.get(ShopPaths.STRING).getPrice() * 3 + shopItems.get(ShopPaths.STRING).getPrice() * 3));
        shopItems.put(ShopPaths.FEATHER, new ShopItemStack(new ItemStack(Material.FEATHER, 1), .0075));
        shopItems.put(ShopPaths.ARROW, new ShopItemStack(new ItemStack(Material.ARROW, 1), (shopItems.get(ShopPaths.FLINT).getPrice() + shopItems.get(ShopPaths.STICK).getPrice() + shopItems.get(ShopPaths.FEATHER).getPrice()) / 4));
        shopItems.put(ShopPaths.IRON_SWORD, new ShopItemStack(new ItemStack(Material.IRON_SWORD, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice()));
        shopItems.put(ShopPaths.CHARCOAL, new ShopItemStack(new ItemStack(Material.COAL, 1, (byte) 1), shopItems.get(ShopPaths.COAL).getPrice() + fuel));
        shopItems.put(ShopPaths.WOODEN_SWORD, new ShopItemStack(new ItemStack(Material.WOOD_SWORD, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice()));
        shopItems.put(ShopPaths.WOODEN_SHOVEL, new ShopItemStack(new ItemStack(Material.WOOD_SPADE, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.WOODEN_PICKAXE, new ShopItemStack(new ItemStack(Material.WOOD_PICKAXE, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.WOODEN_AXE, new ShopItemStack(new ItemStack(Material.WOOD_AXE, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.STONE_SWORD, new ShopItemStack(new ItemStack(Material.STONE_SWORD, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice()));
        shopItems.put(ShopPaths.STONE_SHOVEL, new ShopItemStack(new ItemStack(Material.STONE_SPADE, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.STONE_PICKAXE, new ShopItemStack(new ItemStack(Material.STONE_PICKAXE, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.DIAMOND_SWORD, new ShopItemStack(new ItemStack(Material.DIAMOND_SWORD, 1), shopItems.get(ShopPaths.DIAMOND).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice()));
        shopItems.put(ShopPaths.DIAMOND_SHOVEL, new ShopItemStack(new ItemStack(Material.DIAMOND_SPADE, 1), shopItems.get(ShopPaths.DIAMOND).getPrice() + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.DIAMOND_PICKAXE, new ShopItemStack(new ItemStack(Material.DIAMOND_PICKAXE, 1), shopItems.get(ShopPaths.DIAMOND).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.DIAMOND_AXE, new ShopItemStack(new ItemStack(Material.DIAMOND_AXE, 1), shopItems.get(ShopPaths.DIAMOND).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.BOWL, new ShopItemStack(new ItemStack(Material.BOWL, 1), (shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 3) / 4));
        shopItems.put(ShopPaths.MUSHROOM_STEW, new ShopItemStack(new ItemStack(Material.MUSHROOM_SOUP, 1), shopItems.get(ShopPaths.BOWL).getPrice() + shopItems.get(ShopPaths.RED_MUSHROOM).getPrice() + shopItems.get(ShopPaths.BROWN_MUSHROOM).getPrice()));
        shopItems.put(ShopPaths.GOLDEN_SWORD, new ShopItemStack(new ItemStack(Material.GOLD_SWORD, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice()));
        shopItems.put(ShopPaths.GOLDEN_SHOVEL, new ShopItemStack(new ItemStack(Material.GOLD_SPADE, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.GOLDEN_PICKAXE, new ShopItemStack(new ItemStack(Material.GOLD_PICKAXE, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.GOLDEN_AXE, new ShopItemStack(new ItemStack(Material.GOLD_AXE, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 3 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.WOODEN_HOE, new ShopItemStack(new ItemStack(Material.WOOD_HOE, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.STONE_HOE, new ShopItemStack(new ItemStack(Material.STONE_HOE, 1), shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.IRON_HOE, new ShopItemStack(new ItemStack(Material.IRON_HOE, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.DIAMOND_HOE, new ShopItemStack(new ItemStack(Material.DIAMOND_HOE, 1), shopItems.get(ShopPaths.DIAMOND).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.GOLDEN_HOE, new ShopItemStack(new ItemStack(Material.GOLD_HOE, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 2 + shopItems.get(ShopPaths.STICK).getPrice() * 2));
        shopItems.put(ShopPaths.WHEAT_SEEDS, new ShopItemStack(new ItemStack(Material.SEEDS, 1), .005));
        shopItems.put(ShopPaths.WHEAT, new ShopItemStack(new ItemStack(Material.WHEAT, 1), .015));
        shopItems.put(ShopPaths.BREAD, new ShopItemStack(new ItemStack(Material.BREAD, 1), shopItems.get(ShopPaths.WHEAT).getPrice() * 3));
        shopItems.put(ShopPaths.LEATHER_HELMET, new ShopItemStack(new ItemStack(Material.LEATHER_HELMET, 1), shopItems.get(ShopPaths.LEATHER).getPrice() * 5));
        shopItems.put(ShopPaths.LEATHER_TUNIC, new ShopItemStack(new ItemStack(Material.LEATHER_CHESTPLATE, 1), shopItems.get(ShopPaths.LEATHER).getPrice() * 8));
        shopItems.put(ShopPaths.LEATHER_PANTS, new ShopItemStack(new ItemStack(Material.LEATHER_LEGGINGS, 1), shopItems.get(ShopPaths.LEATHER).getPrice() * 7));
        shopItems.put(ShopPaths.LEATHER_BOOTS, new ShopItemStack(new ItemStack(Material.LEATHER_BOOTS, 1), shopItems.get(ShopPaths.LEATHER).getPrice() * 4));
        shopItems.put(ShopPaths.IRON_HELMET, new ShopItemStack(new ItemStack(Material.IRON_HELMET, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 5));
        shopItems.put(ShopPaths.IRON_CHESTPLATE, new ShopItemStack(new ItemStack(Material.IRON_CHESTPLATE, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 8));
        shopItems.put(ShopPaths.IRON_LEGGINGS, new ShopItemStack(new ItemStack(Material.IRON_LEGGINGS, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 7));
        shopItems.put(ShopPaths.IRON_BOOTS, new ShopItemStack(new ItemStack(Material.IRON_BOOTS, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 4));
        shopItems.put(ShopPaths.GOLDEN_HELMET, new ShopItemStack(new ItemStack(Material.GOLD_HELMET, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 5));
        shopItems.put(ShopPaths.GOLDEN_CHESTPLATE, new ShopItemStack(new ItemStack(Material.GOLD_CHESTPLATE, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 8));
        shopItems.put(ShopPaths.GOLDEN_LEGGINGS, new ShopItemStack(new ItemStack(Material.GOLD_LEGGINGS, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 7));
        shopItems.put(ShopPaths.GOLDEN_BOOTS, new ShopItemStack(new ItemStack(Material.GOLD_BOOTS, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 4));
        shopItems.put(ShopPaths.RAW_PORKCHOP, new ShopItemStack(new ItemStack(Material.PORK, 1), .03));
        shopItems.put(ShopPaths.COOKED_PORKCHOP, new ShopItemStack(new ItemStack(Material.GRILLED_PORK, 1), .08));
        shopItems.put(ShopPaths.PAINTING, new ShopItemStack(new ItemStack(Material.PAINTING, 1), (shopItems.get(ShopPaths.STICK).getPrice() * 8) + shopItems.get(ShopPaths.WHITE_WOOL).getPrice()));
        shopItems.put(ShopPaths.APPLE, new ShopItemStack(new ItemStack(Material.APPLE, 1), .04));
        shopItems.put(ShopPaths.GOLDEN_APPLE, new ShopItemStack(new ItemStack(Material.GOLDEN_APPLE, 1), (shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 8) + shopItems.get(ShopPaths.APPLE).getPrice()));
        shopItems.put(ShopPaths.ENCHANTED_GOLDEN_APPLE, new ShopItemStack(new ItemStack(Material.GOLDEN_APPLE, 1, (byte) 1), (shopItems.get(ShopPaths.GOLD_BLOCK).getPrice() * 8) + shopItems.get(ShopPaths.APPLE).getPrice()));
        shopItems.put(ShopPaths.BUCKET, new ShopItemStack(new ItemStack(Material.BUCKET, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 3));
        shopItems.put(ShopPaths.WATER_BUCKET, new ShopItemStack(new ItemStack(Material.WATER_BUCKET, 1), shopItems.get(ShopPaths.BUCKET).getPrice() + .0075));
        shopItems.put(ShopPaths.LAVA_BUCKET, new ShopItemStack(new ItemStack(Material.LAVA_BUCKET, 1), shopItems.get(ShopPaths.BUCKET).getPrice() + .015));
        shopItems.put(ShopPaths.MINECART, new ShopItemStack(new ItemStack(Material.MINECART, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 5));
        shopItems.put(ShopPaths.SADDLE, new ShopItemStack(new ItemStack(Material.SADDLE, 1), .25));
        shopItems.put(ShopPaths.BOAT, new ShopItemStack(new ItemStack(Material.BOAT, 1), shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 5));
        shopItems.put(ShopPaths.MILK_BUCKET, new ShopItemStack(new ItemStack(Material.MILK_BUCKET, 1), shopItems.get(ShopPaths.BUCKET).getPrice()));
        shopItems.put(ShopPaths.MINECART_WITH_CHEST, new ShopItemStack(new ItemStack(Material.STORAGE_MINECART, 1), shopItems.get(ShopPaths.MINECART).getPrice() + shopItems.get(ShopPaths.CHEST).getPrice()));
        shopItems.put(ShopPaths.MINECART_WITH_FURNACE, new ShopItemStack(new ItemStack(Material.POWERED_MINECART, 1), shopItems.get(ShopPaths.MINECART).getPrice() + shopItems.get(ShopPaths.FURNACE).getPrice()));
        shopItems.put(ShopPaths.EGG, new ShopItemStack(new ItemStack(Material.EGG, 1), .01));
        shopItems.put(ShopPaths.COMPASS, new ShopItemStack(new ItemStack(Material.COMPASS, 1), (shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 4) + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.FISHING_ROD, new ShopItemStack(new ItemStack(Material.FISHING_ROD, 1), (shopItems.get(ShopPaths.STICK).getPrice() * 3) + (shopItems.get(ShopPaths.STRING).getPrice() * 2)));
        shopItems.put(ShopPaths.CLOCK, new ShopItemStack(new ItemStack(Material.WATCH, 1), (shopItems.get(ShopPaths.GOLD_INGOT).getPrice() * 4) + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.RAW_FISH, new ShopItemStack(new ItemStack(Material.RAW_FISH, 1), .02));
        shopItems.put(ShopPaths.RAW_SALMON, new ShopItemStack(new ItemStack(Material.RAW_FISH, 1, (byte) 1), .02));
        shopItems.put(ShopPaths.CLOWNFISH, new ShopItemStack(new ItemStack(Material.RAW_FISH, 1, (byte) 2), .01));
        shopItems.put(ShopPaths.PUFFERFISH, new ShopItemStack(new ItemStack(Material.RAW_FISH, 1, (byte) 3), .01));
        shopItems.put(ShopPaths.COOKED_FISH, new ShopItemStack(new ItemStack(Material.COOKED_FISH, 1), .05));
        shopItems.put(ShopPaths.COOKED_SALMON, new ShopItemStack(new ItemStack(Material.COOKED_FISH, 1, (byte) 1), .06));
        shopItems.put(ShopPaths.CAKE, new ShopItemStack(new ItemStack(Material.CAKE, 1), (shopItems.get(ShopPaths.SUGAR).getPrice() * 2) + shopItems.get(ShopPaths.EGG).getPrice() + (shopItems.get(ShopPaths.WHEAT).getPrice() * 3)));
        shopItems.put(ShopPaths.BED, new ShopItemStack(new ItemStack(Material.BED, 1), (shopItems.get(ShopPaths.WHITE_WOOL).getPrice() * 3) + (shopItems.get(ShopPaths.OAK_WOOD_PLANK).getPrice() * 3)));
        shopItems.put(ShopPaths.REDSTONE_REPEATER, new ShopItemStack(new ItemStack(Material.DIODE, 1), (shopItems.get(ShopPaths.STONE).getPrice() * 3) + (shopItems.get(ShopPaths.REDSTONE_TORCH_ON).getPrice() * 2) + shopItems.get(ShopPaths.REDSTONE).getPrice()));
        shopItems.put(ShopPaths.COOKIE, new ShopItemStack(new ItemStack(Material.COOKIE, 1), ((shopItems.get(ShopPaths.WHEAT).getPrice() * 2) + shopItems.get(ShopPaths.COCO_BEANS).getPrice()) / 8));
        shopItems.put(ShopPaths.MAP, new ShopItemStack(new ItemStack(Material.EMPTY_MAP, 1), shopItems.get(ShopPaths.PAPER).getPrice() * 8 + shopItems.get(ShopPaths.COMPASS).getPrice()));
        shopItems.put(ShopPaths.SHEARS, new ShopItemStack(new ItemStack(Material.SHEARS, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 2));
        shopItems.put(ShopPaths.PUMPKIN_SEEDS, new ShopItemStack(new ItemStack(Material.PUMPKIN_SEEDS, 1), shopItems.get(ShopPaths.PUMPKIN).getPrice() * 4));
        shopItems.put(ShopPaths.MELON_SEEDS, new ShopItemStack(new ItemStack(Material.MELON_SEEDS, 1), shopItems.get(ShopPaths.MELON).getPrice()));
        shopItems.put(ShopPaths.RAW_BEEF, new ShopItemStack(new ItemStack(Material.RAW_BEEF, 1), .03));
        shopItems.put(ShopPaths.STEAK, new ShopItemStack(new ItemStack(Material.COOKED_BEEF, 1), .08));
        shopItems.put(ShopPaths.RAW_CHICKEN, new ShopItemStack(new ItemStack(Material.RAW_CHICKEN, 1), .01));
        shopItems.put(ShopPaths.COOKED_CHICKEN, new ShopItemStack(new ItemStack(Material.COOKED_CHICKEN, 1), .06));
        shopItems.put(ShopPaths.ROTTEN_FLESH, new ShopItemStack(new ItemStack(Material.ROTTEN_FLESH, 1), .01));
        shopItems.put(ShopPaths.GHAST_TEAR, new ShopItemStack(new ItemStack(Material.GHAST_TEAR, 1), .1));
        shopItems.put(ShopPaths.GOLD_NUGGET, new ShopItemStack(new ItemStack(Material.GOLD_NUGGET, 1), shopItems.get(ShopPaths.GOLD_INGOT).getPrice() / 9));
        shopItems.put(ShopPaths.NETHER_WART, new ShopItemStack(new ItemStack(Material.NETHER_STALK, 1), .02));
        shopItems.put(ShopPaths.GLASS_BOTTLE, new ShopItemStack(new ItemStack(Material.GLASS_BOTTLE, 1), shopItems.get(ShopPaths.GLASS).getPrice()));
        shopItems.put(ShopPaths.SPIDER_EYE, new ShopItemStack(new ItemStack(Material.SPIDER_EYE, 1), .0075));
        shopItems.put(ShopPaths.FERMENTED_SPIDER_EYE, new ShopItemStack(new ItemStack(Material.FERMENTED_SPIDER_EYE, 1), shopItems.get(ShopPaths.BROWN_MUSHROOM).getPrice() + shopItems.get(ShopPaths.SPIDER_EYE).getPrice() + shopItems.get(ShopPaths.SUGAR).getPrice()));
        shopItems.put(ShopPaths.MAGMA_CREAM, new ShopItemStack(new ItemStack(Material.MAGMA_CREAM, 1), .015));
        shopItems.put(ShopPaths.BREWING_STAND, new ShopItemStack(new ItemStack(Material.BREWING_STAND_ITEM, 1), (shopItems.get(ShopPaths.COBBLESTONE).getPrice() * 3) + shopItems.get(ShopPaths.BLAZE_ROD).getPrice()));
        shopItems.put(ShopPaths.CAULDRON, new ShopItemStack(new ItemStack(Material.CAULDRON_ITEM, 1), shopItems.get(ShopPaths.IRON_INGOT).getPrice() * 7));
        shopItems.put(ShopPaths.GLISTERING_MELON, new ShopItemStack(new ItemStack(Material.SPECKLED_MELON, 1), (shopItems.get(ShopPaths.GOLD_NUGGET).getPrice() * 8) + shopItems.get(ShopPaths.MELON).getPrice()));
        shopItems.put(ShopPaths.FIRE_CHARGE, new ShopItemStack(new ItemStack(Material.FIREBALL, 1), shopItems.get(ShopPaths.BLAZE_POWDER).getPrice() + shopItems.get(ShopPaths.GUNPOWDER).getPrice() + shopItems.get(ShopPaths.COAL).getPrice()));
        shopItems.put(ShopPaths.BOOK_AND_QUILL, new ShopItemStack(new ItemStack(Material.BOOK_AND_QUILL, 1), shopItems.get(ShopPaths.BOOK).getPrice() + shopItems.get(ShopPaths.INK_SACK).getPrice() + shopItems.get(ShopPaths.FEATHER).getPrice()));
        shopItems.put(ShopPaths.ITEM_FRAME, new ShopItemStack(new ItemStack(Material.ITEM_FRAME, 1), shopItems.get(ShopPaths.STICK).getPrice() * 8 + shopItems.get(ShopPaths.LEATHER).getPrice()));
        shopItems.put(ShopPaths.FLOWER_POT, new ShopItemStack(new ItemStack(Material.FLOWER_POT_ITEM, 1), shopItems.get(ShopPaths.BRICK).getPrice() * 3));
        shopItems.put(ShopPaths.CARROT, new ShopItemStack(new ItemStack(Material.CARROT_ITEM, 1), .015));
        shopItems.put(ShopPaths.POTATO, new ShopItemStack(new ItemStack(Material.POTATO_ITEM, 1), .015));
        shopItems.put(ShopPaths.BAKED_POTATO, new ShopItemStack(new ItemStack(Material.BAKED_POTATO, 1), shopItems.get(ShopPaths.POTATO).getPrice() + fuel));
        shopItems.put(ShopPaths.POISONOUS_POTATO, new ShopItemStack(new ItemStack(Material.POISONOUS_POTATO, 1), .0075));
        shopItems.put(ShopPaths.GOLDEN_CARROT, new ShopItemStack(new ItemStack(Material.GOLDEN_CARROT, 1), (shopItems.get(ShopPaths.GOLD_NUGGET).getPrice() * 8) + shopItems.get(ShopPaths.CARROT).getPrice()));
        shopItems.put(ShopPaths.CARROT_ON_A_STICK, new ShopItemStack(new ItemStack(Material.CARROT_STICK, 1), shopItems.get(ShopPaths.FISHING_ROD).getPrice() + shopItems.get(ShopPaths.CARROT).getPrice()));
        shopItems.put(ShopPaths.NETHER_STAR, new ShopItemStack(new ItemStack(Material.NETHER_STAR, 1), 20.0));
        shopItems.put(ShopPaths.PUMPKIN_PIE, new ShopItemStack(new ItemStack(Material.PUMPKIN_PIE, 1), shopItems.get(ShopPaths.PUMPKIN).getPrice() + shopItems.get(ShopPaths.EGG).getPrice() + shopItems.get(ShopPaths.SUGAR).getPrice()));
        shopItems.put(ShopPaths.REDSTONE_COMPARATOR, new ShopItemStack(new ItemStack(Material.REDSTONE_COMPARATOR, 1), (shopItems.get(ShopPaths.STONE).getPrice() * 3) + (shopItems.get(ShopPaths.REDSTONE_TORCH_ON).getPrice() * 3) + shopItems.get(ShopPaths.NETHER_QUARTZ).getPrice()));
        shopItems.put(ShopPaths.MINECART_WITH_TNT, new ShopItemStack(new ItemStack(Material.EXPLOSIVE_MINECART, 1), shopItems.get(ShopPaths.MINECART).getPrice() + shopItems.get(ShopPaths.TNT).getPrice()));
        shopItems.put(ShopPaths.MINECART_WITH_HOPPER, new ShopItemStack(new ItemStack(Material.HOPPER_MINECART, 1), shopItems.get(ShopPaths.MINECART).getPrice() + shopItems.get(ShopPaths.CAULDRON).getPrice()));
        shopItems.put(ShopPaths.RAW_RABBIT, new ShopItemStack(new ItemStack(Material.RABBIT, 1), .02));
        shopItems.put(ShopPaths.COOKED_RABBIT, new ShopItemStack(new ItemStack(Material.COOKED_RABBIT, 1), .05));
        shopItems.put(ShopPaths.RABBIT_STEW, new ShopItemStack(new ItemStack(Material.RABBIT_STEW, 1), shopItems.get(ShopPaths.COOKED_RABBIT).getPrice() + shopItems.get(ShopPaths.CARROT).getPrice() + shopItems.get(ShopPaths.BAKED_POTATO).getPrice() + shopItems.get(ShopPaths.RED_MUSHROOM).getPrice() + shopItems.get(ShopPaths.BOWL).getPrice()));
        shopItems.put(ShopPaths.RABBITS_FOOT, new ShopItemStack(new ItemStack(Material.RABBIT_FOOT, 1), .25));
        shopItems.put(ShopPaths.RABBIT_HIDE, new ShopItemStack(new ItemStack(Material.RABBIT_HIDE, 1), shopItems.get(ShopPaths.LEATHER).getPrice() / 4));
        shopItems.put(ShopPaths.ARMOR_STAND, new ShopItemStack(new ItemStack(Material.ARMOR_STAND, 1), shopItems.get(ShopPaths.STICK).getPrice() * 6 + shopItems.get(ShopPaths.STONE_SLAB).getPrice()));
        shopItems.put(ShopPaths.LEAD, new ShopItemStack(new ItemStack(Material.LEASH, 1), ((shopItems.get(ShopPaths.STRING).getPrice() * 4) + shopItems.get(ShopPaths.SLIMEBALL).getPrice()) / 2));

        shopItems = (HashMap) sortMap(shopItems);
        ArrayList<Map.Entry<String, ShopItemStack>> items = new ArrayList<>(shopItems.entrySet());

        items.sort(new IdComparator());
    }

    private void loadShopItems(ConfigurationSection configurationSection) {
        for (String string : configurationSection.getKeys(false)) {
            ConfigurationSection itemSection = configurationSection.getConfigurationSection(string);
            if (itemSection.getString(ShopPaths.SHOP_ITEM_TYPE).equals(ShopPaths.ITEM_STACK)) {
                shopItems.put(string, new ShopItemStack(ItemUtil.getItemStack(itemSection), itemSection.getDouble(ShopPaths.COST)));
            }
            if (itemSection.getString(ShopPaths.SHOP_ITEM_TYPE).equals(ShopPaths.PERMISSION)) {
//                shopItems.put(string, new ShopPermission(new Permission(itemSection.getString(ShopPaths.PERMISSION)), itemSection.getDouble(ShopPaths.COST)));
            }
        }
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.getClickedInventory() != null) {
            if (event.getClickedInventory().getName().startsWith("Shop Page")) {
                event.setCancelled(true);

                int page = Integer.valueOf(event.getClickedInventory().getName().split(" ")[2]);

                if (event.getSlot() == 45) {
                    page--;
                }
                if (event.getSlot() == 53) {
                    page++;
                }
                if (page <= 0) {
                    page = getShopPages().size();
                }
                if (page > getShopPages().size()) {
                    page = 1;
                }
                event.getWhoClicked().openInventory(getShopPages().get(page - 1));
            }
        }
    }

    private Map<String, ShopItemStack> sortMap(
            Map<String, ShopItemStack> unsortedMap) {

        List<Map.Entry<String, ShopItemStack>> list = new LinkedList<>(unsortedMap.entrySet());

        Collections.sort(list,
                new Comparator<Map.Entry<String, ShopItemStack>>() {

                    @Override
                    public int compare(
                            Map.Entry<String, ShopItemStack> o1,
                            Map.Entry<String, ShopItemStack> o2) {

                        return o1.getValue().getItemStack().getTypeId() - o2.getValue().getItemStack().getTypeId();
                    }
                });

        Map<String, ShopItemStack> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, ShopItemStack> item : list) {
            sortedMap.put(item.getKey(), item.getValue());
        }
        return sortedMap;
    }

    class IdComparator
            implements Comparator<Map.Entry<String, ShopItemStack>> {
        public int compare(Map.Entry<String, ShopItemStack> a,
                           Map.Entry<String, ShopItemStack> b) {
            return b.getValue().getItemStack().getTypeId() - b.getValue().getItemStack().getTypeId();
        }
    }
}
