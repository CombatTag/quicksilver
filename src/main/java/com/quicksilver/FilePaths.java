package com.quicksilver;

import java.io.File;

/**
 * Created by Derek on 12/3/2014.
 * Time: 7:40 PM
 */
public abstract class FilePaths {

    private static final File DATA_FOLDER = QuickSilver.p.getDataFolder();
    public static final File USER_DATA_FOLDER = new File(DATA_FOLDER, "userData");
}
