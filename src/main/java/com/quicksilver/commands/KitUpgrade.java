package com.quicksilver.commands;

import com.quicksilver.Kit;
import com.quicksilver.QuickSilver;
import com.quicksilver.user.User;
import com.quicksilver.utils.TierUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

import static com.quicksilver.utils.ChatUtil.t;

/**
 * Created by Derek on 12/7/2014.
 * Time: 7:49 PM
 */
public class KitUpgrade implements TabExecutor {

    private String PREFIX = "\u00bb ";

    @Override
    public boolean onCommand(final CommandSender commandSender, Command command, String label, String[] args) {
        if (commandSender == QuickSilver.p.getServer().getConsoleSender()) {
            PREFIX = "> ";
            commandSender.sendMessage(t("noConsole"));
            return true;
        }

        Player player = (Player) commandSender;

        User user = QuickSilver.p.getUserManager().getUser(player);

        Kit kit = user.getBaseKit();

        ItemStack sword = TierUtil.getNextTier(kit.getSword());
        ItemStack helmet = TierUtil.getNextTier(kit.getHelmet());
        ItemStack chestPlate = TierUtil.getNextTier(kit.getChestplate());
        ItemStack leggings = TierUtil.getNextTier(kit.getLeggings());
        ItemStack boots = TierUtil.getNextTier(kit.getBoots());

        if (sword != null && helmet != null && chestPlate != null && leggings != null && boots != null) {
            kit.setSword(sword);
            kit.setHelmet(helmet);
            kit.setChestplate(chestPlate);
            kit.setLeggings(leggings);
            kit.setBoots(boots);
            user.setBaseKit(kit);
            commandSender.sendMessage(t("kitUpgrade"));
        } else {
            commandSender.sendMessage(t("kitHigh"));
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String alias, String[] args) {
        return new ArrayList<>();
    }
}
