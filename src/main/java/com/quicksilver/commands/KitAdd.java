package com.quicksilver.commands;

import com.quicksilver.QuickSilver;
import com.quicksilver.user.User;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static com.quicksilver.utils.ChatUtil.t;

/**
 * Created by Derek on 12/7/2014.
 * Time: 7:32 PM
 */
public class KitAdd implements TabExecutor {

    private String PREFIX = "\u00bb ";

    @Override
    public boolean onCommand(final CommandSender commandSender, Command command, String label, String[] args) {
        if (commandSender == QuickSilver.p.getServer().getConsoleSender()) {
            PREFIX = "> ";

            commandSender.sendMessage(t("noConsole"));
            return true;
        }

        Player player = (Player) commandSender;

        User user = QuickSilver.p.getUserManager().getUser(player);

        user.addKit(user.getNewKit());

        commandSender.sendMessage(t("kitAdd"));

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String alias, String[] args) {
        return new ArrayList<>();
    }
}
