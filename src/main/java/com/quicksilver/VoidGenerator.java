package com.quicksilver;

import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

/**
 * Created by Derek on 12/3/2014.
 * Time: 6:35 PM
 */
public class VoidGenerator extends ChunkGenerator {

    public byte[][] generateBlockSections(World world, Random random, int chunkX, int chunkZ, BiomeGrid biomeGrid) {
        return new byte[world.getMaxHeight() / 16][];
    }
}
